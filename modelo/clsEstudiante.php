<?php 
include_once('clsPersona.php');
/*Clase Estudiante se encarga de gestionar todas las acciones referente a los estidantes;
- Crear.
- Editar.
- Consultar.
- Eliminar.
La clase estudiante hereda de la clase persona.
*/
class Estudiante extends Persona{
	
	function __construct()
	{
		# code...
	}

	function CrearEstudiante($primerApellidoEst, $segundoApellidoEst , $primerNombreEst , $segundoNombreEst , $nacEst, $cedulaEst, $idgrado2, $idseccion2, $idturno2 , $sexoEst, $fecNacEst, $lugarNacEst, $direccionEst , $telfhabEst, $telfCelEst, $enfermedadEst, $enfespecifica, $hermanos, $gradoHerm, $seccionHerm, $anioEscolar, $repitiente, $rutaImagen){
		$sql=" INSERT INTO estudiante(
            p_apellido_est, s_apellido_est, p_nombre_est, s_nombre_est, 
            nac_est, ci_est, idgrado, idseccion, idturno, sexo_est, fec_nac_est, lugar_nac_est, 
            direccion, telf_hab_est, telf_cel_est, enfermedad, des_enfermedad, 
            hermanos, grado_herm, seccion_herm, periodo, repitiente, 
            foto, estatus_est)
    VALUES ( '$primerApellidoEst', '$segundoApellidoEst' , '$primerNombreEst' , '$segundoNombreEst' , '$nacEst', '$cedulaEst', '$idgrado2', '$idseccion2', '$idturno2', '$sexoEst', '$fecNacEst', '$lugarNacEst', '$direccionEst' , '$telfhabEst', '$telfCelEst', '$enfermedadEst', '$enfespecifica', '$hermanos', '$gradoHerm', '$seccionHerm', '$anioEscolar', '$repitiente', '$rutaImagen', 'activo' )";
		//echo $sql;die;
		$objetoEstudiante = new Datos();
		$objetoEstudiante->filtro($sql);
		$objetoEstudiante->cerrarConexion();
		return;
	}
	function selectEstudiante($var, $var1, $idPersona){
                //echo $idPersona;
		$sql = "SELECT $var FROM estudiante WHERE $var1 = $idPersona";
		//echo $sql;die;
		$objetoEstudiante = new Datos();
		$datos = $objetoEstudiante->filtro($sql);
		$arreglo = $objetoEstudiante->proximo($datos);
		$objetoEstudiante->cerrarFiltro($datos);
		$objetoEstudiante->cerrarConexion();
		return $arreglo[$var];
	}
	function editarEstudiante($id, $grado, $seccion, $repitiente, $turno, $nacEst, $fechaNac, $direccion){
		$sql="UPDATE estudiante SET idgrado='$grado', idseccion='$seccion', repitiente='$repitiente',turno='$turno', fec_nac_est='$fechaNac', direccion= '$direccion'
			WHERE id = '$id'";
		//echo $sql;die;
			$objPer = new Datos();
			$datos = $objPer->filtro($sql);
			$objPer->cerrarConexion();
		}
	public function eliminarEstudiante($id){
			$objDatos = new Datos();
			$sql = "UPDATE estudiante SET estatus_est= 'retirado' WHERE id = '$id'";
			//echo $sql;die;	
			$objDatos->filtro($sql);
			$objDatos->cerrarConexion();
			}
	function datosCompletos ($id){
		$sql="Select id, p_apellido_est, s_apellido_est, p_nombre_est, s_nombre_est, nac_est, ci_est,
                              grado, seccion, turno, repitiente, fec_nac_est, direccion, foto from estudiante T1
                              inner join grado T2 on T1.idgrado=T2.idgrado
                              inner join seccion T3 on T1.idseccion=T3.idseccion
                              inner join turno T4 on t1.idturno=T4.idturno
                              where id='$id'";
            
            
//            $sql = "SELECT 'primerNombre','segundoNombre','primerApellido','segundoApellido','cedulaIdentidad','direccionHab','nacionalidad','fechaNac','foto','turno', 'seccion', 'grado', 'idestudiante','repitiente'
//		FROM persona T1 
//		INNER JOIN estudiante T2 ON T1.idpersona = T2.per_idpersona
//		INNER JOIN turno T3 ON T2.tur_idturno = T3.idturno
//		INNER JOIN seccion T4 ON T2.seccion_idseccion = T4.idseccion
//		INNER JOIN grado T5 ON T2.grado_idgrado = T5.idgrado
//		WHERE T2.idestudiante = $idEstudiante";
                
                
		$objetoEstudiante = new Datos();
		$datos = $objetoEstudiante->filtro($sql);
		$arreglo = $objetoEstudiante->proximo($datos);
		$objetoEstudiante->cerrarFiltro($datos);
		$objetoEstudiante->cerrarConexion();
		return $arreglo;
	}
	public function listarEstudiantes(){
			$objDatos = new Datos();
                        $sql="Select id, p_apellido_est, s_apellido_est, p_nombre_est, s_nombre_est, 
                              grado, seccion, turno from estudiante T1
                              inner join grado T2 on T1.idgrado=T2.idgrado
                              inner join seccion T3 on T1.idseccion=T3.idseccion
                              inner join turno T4 on t1.idturno=T4.idturno
                              where estatus_est='activo'";
                        
//			$sql = "SELECT 'primerNombre','segundoNombre','primerApellido','segundoApellido','turno', 'seccion', 'grado', 'idestudiante'
//					FROM persona T1
//					INNER JOIN estudiante T2  ON T1.idpersona = T2.per_idpersona
//					INNER JOIN turno T3 ON T2.tur_idturno = T3.idturno
//					INNER JOIN seccion T4 ON T2.seccion_idseccion = T4.idseccion
//					INNER JOIN grado T5 ON T2.grado_idgrado = T5.idgrado
//					WHERE T2.status = 'activo'";
			//echo $sql;die;
			$datos = $objDatos->filtro($sql);
			$objDatos->cerrarConexion();
			return $datos;
			}
	public function fichaEstudiante($cedulaEst){
		$objetoEstudiante = new Datos();
		$sql = "SELECT id, p_apellido_est, s_apellido_est, p_nombre_est, s_nombre_est, 
                        nac_est, ci_est, grado, seccion, turno, fec_nac_est, 
                        direccion, repitiente,foto
		FROM estudiante T1
                inner join grado T2 on T1.idgrado=T2.idgrado
                inner join seccion T3 on T1.idseccion=T3.idseccion
                inner join turno T4 on t1.idturno=T4.idturno
                WHERE ci_est = $cedulaEst";
		//echo $sql;die;	
		$objetoEstudiante = new Datos();
		$datos = $objetoEstudiante->filtro($sql);
		$arreglo = $objetoEstudiante->proximo($datos);
		$objetoEstudiante->cerrarFiltro($datos);
		$objetoEstudiante->cerrarConexion();
		return $arreglo;
	}
}
 ?>