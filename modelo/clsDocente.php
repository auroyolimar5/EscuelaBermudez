<?php 
include_once('clsDatos.php');

/**
* 
*/
class Docente
{
	
	/*function __construct()
	{
		# code...
	}*/

	function crearDocente($nac_profesor, $cedula_prof, $p_apellido_prof, $s_apellido_prof, 
            $p_nombre_prof, $s_nombre_prof, $sexo_prof, $fecha_nac_prof, $lugar_nac_prof, 
            $direccion_prof, $telf_hab_prof, $telf_cel_prof, $idgrado_prof, $idseccion_prof, 
            $idturno_prof, $email_prof, $foto_prof){
		$sql="INSERT INTO profesores (nac_profesor, cedula_prof, p_apellido_prof, s_apellido_prof, 
                     p_nombre_prof, s_nombre_prof, sexo_prof, fecha_nac_prof, lugar_nac_prof, 
                     direccion_prof, telf_hab_prof, telf_cel_prof, idgrado_prof, idseccion_prof, 
                     idturno_prof, email_prof, foto_prof, estatus_prof) 
                VALUES ('$nac_profesor', '$cedula_prof', '$p_apellido_prof', '$s_apellido_prof', 
                        '$p_nombre_prof', '$s_nombre_prof', '$sexo_prof', '$fecha_nac_prof', '$lugar_nac_prof', 
                        '$direccion_prof', '$telf_hab_prof', '$telf_cel_prof', '$idgrado_prof', '$idseccion_prof', 
                        '$idturno_prof', '$email_prof', '$foto_prof', 'activo')";
		//echo $sql;die;
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();
	}
	function eliminarDocente($id){
			$objDatos = new Datos();
			$sql = "UPDATE profesores SET estatus_prof='retirado' WHERE id = '$id'";
			//echo $sql;die;	
			$objDatos->filtro($sql);
			$objDatos->cerrarConexion();
			}
	function asociarGrado($idDocente,$grado){
		$sql="INSERT INTO `profesor_grado`(`pro_idprofesor`, `gra_idgrado`) VALUES ($idDocente,$grado)";
		//echo $sql;die;
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();
	}
	function asociarSeccion($idDocente,$seccion){
		$sql="INSERT INTO `profesor_seccion`(`prof_idprofesor`, `sec_idseccion`) VALUES ($idDocente,$seccion)";
		//echo $sql;die;
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();	
	}
	function asociarturno($idDocente,$turno){
		$sql="INSERT INTO `profesor_turno`(`pro_idprofesor`, `tur_idturno`) VALUES ($idDocente,$turno)";
		//echo $sql;die;
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();	
	}
	function editarGrado($idDocente,$grado){
		$sql="UPDATE `profesor_grado` SET `gra_idgrado`= $grado WHERE `pro_idprofesor` = $idDocente";
		//echo $sql;die;
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();
	}
	function editarSeccion($idDocente,$seccion){
		$sql="UPDATE `profesor_seccion` SET `sec_idseccion`= $seccion WHERE `prof_idprofesor`= $idDocente";
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();
	}
	function editarturno($idDocente,$turno){
		$sql="UPDATE `profesor_turno` SET `tur_idturno`= $turno WHERE `pro_idprofesor`= $idDocente";
		$objDocente = new Datos();
		$objDocente->filtro($sql);
		$objDocente->cerrarConexion();
	}
	function selectDocente($cedula_prof){
		$sql = "SELECT nac_profesor, cedula_prof, p_apellido_prof, s_apellido_prof, 
                     p_nombre_prof, s_nombre_prof, fecha_nac_prof, direccion_prof, telf_hab_prof, telf_cel_prof,
                     grado, seccion, turno, email_prof, foto_prof
                     FROM profesores T1
                     inner join grado T2 on T1.idgrado_prof=T2.idgrado
                     inner join seccion T3 on T1.idseccion_prof=T3.idseccion
                     inner join turno T4 on T1.idturno_prof=T4.idturno
                     WHERE cedula_prof = $cedula_prof";
		//echo $sql;die;
		$objDocente = new Datos();
		$datos = $objDocente->filtro($sql);
		$arreglo = $objDocente->proximo($datos);
		$objDocente->cerrarFiltro($datos);
		$objDocente->cerrarConexion();
		return $arreglo;
	}
	function listaDocentes(){
		$sql="SELECT id, nac_profesor, cedula_prof, p_apellido_prof, s_apellido_prof, 
                     p_nombre_prof, s_nombre_prof, fecha_nac_prof, direccion_prof, telf_hab_prof, telf_cel_prof,
                     grado, seccion, turno, email_prof, foto_prof
                     FROM profesores T1
                     inner join grado T2 on T1.idgrado_prof=T2.idgrado
                     inner join seccion T3 on T1.idseccion_prof=T3.idseccion
                     inner join turno T4 on T1.idturno_prof=T4.idturno
                     Where estatus_prof='activo'
                     order by grado, seccion asc";
		//echo $sql; die;
		$objDocente = new Datos();
		//$datos = $objDocente->filtro(str_replace('`','',str_replace('\r\n',' ', $sql)));
		$datos=$objDocente->filtro($sql);
                $objDocente->cerrarConexion();
		return $datos;
	}
	function datosCompletos($id){
		$sql = "SELECT nac_profesor, cedula_prof, p_apellido_prof, s_apellido_prof, 
                     p_nombre_prof, s_nombre_prof, fecha_nac_prof, direccion_prof, telf_hab_prof, telf_cel_prof,
                     grado, seccion, turno, email_prof, foto_prof
                     FROM profesores T1
                     inner join grado T2 on T1.idgrado_prof=T2.idgrado
                     inner join seccion T3 on T1.idseccion_prof=T3.idseccion
                     inner join turno T4 on T1.idturno_prof=T4.idturno
                     WHERE id = $id";
		//echo $sql;die;
		$objDocente = new Datos();
		$datos = $objDocente->filtro(str_replace('`','',str_replace('\r\n',' ', $sql)));
		$arreglo = $objDocente->proximo($datos);
		$objDocente->cerrarFiltro($datos);
		$objDocente->cerrarConexion();
		return $arreglo;
	}
}
?>