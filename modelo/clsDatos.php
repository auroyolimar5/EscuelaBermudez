<?php 
/******************************************************************************************************************
Clase Datos:
la Clase Datos hace las operaciones principales del sistema; conecta y desconecta la bd, guarda, edita, consulta en 
lista e individual, cuenta y elimina datos. También limpuia la cache de quialquier dato
 previamente usado por motor de la bd
___________________________________________________________________________________________________________________
variables:
$conn: "de tipo objeto, contiene la conexión a la bd"
___________________________________________________________________________________________________________________
Métodos:
1) __contruct: incializa las variables ($servidor, $usuario, $clave, $base) con los q se hace la conexión a ala bd
	guadando la misma en ela variable global $conn.

2) __destruct: Limpa los datos almacenados en el construct

3) filtro: recibe una variable $sql que seria una sentencia sql (SELECT, INSERT, UPDATE, DELETE) y la ejecuta usando 
	la variable global $conn, si hay algun problema muestra el error por medio de la función Mysql_error y muere.
	
4) cerrarFiltro: recive la variable $datos y los elimina por medio de la funcion Mysql_free_result para limpiar cache.

5) proximo: recive la variable $datos con la que se ejecuta la función Mysql_fetch_array y devuelve un arreglo de datos.

6) contar: recive la variable $datos y ejecuta la función Mysql_num_rows y devuelve la variable $cantidad. Cuenta filas
	de una tabla.

7) cerrarConexión: Cierra la conexión al a bd.
___________________________________________________________________________________________________________________

******************************************************************************************************************/
class Datos{
	private $conn;

	public function __construct(){
		$sevidor = "localhost";
		$usuario = "postgres";
		$clave = "123";
		$base = "escuela";		
		
		$conn="host=" . $sevidor . " dbname=". $base . " port=5432" . " user=" . $usuario . " password=" .$clave ;
		//establecemos el link
		$this->conn=pg_connect($conn);

		}

	public function __destruct(){
		}
	
	public function filtro($sql){
		$datos = pg_query($this->conn,str_replace("`","",$sql)) or die(pg_last_error() . ' ' . str_replace("`","",$sql) . ' ');
		
		//$datos = mysql_query($sql, $this->conn) ;
		return $datos;
		}
		
	public function cerrarFiltro($datos){
		pg_free_result($datos);
		}
	
	public function proximo($datos){
		$arreglo = pg_fetch_array($datos);
		return $arreglo;
		}

	public function listaAsoc($datos){
   		$array = pg_fetch_assoc($datos);
   		return $array;
  		}
	
	public function contar($datos){
		$cantidad = pg_num_rows($datos);
		return $cantidad;
		}
			
	public function cerrarConexion(){
		pg_close($this->conn);
	}
}

?>