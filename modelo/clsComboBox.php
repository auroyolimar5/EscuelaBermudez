<?php 
include_once('clsDatos.php');
/**
* Esta clase sirve para llevar datos de una tabla específica y colocarlos en el select determinado.
*/
class ComboBox
{
	var $code = "";
	
	function cargarProfesion(){
		$sql = "SELECT idprofecionoficio, profecionoficio FROM profecionoficio";
		$objeProf = new Datos();
		$datos = $objeProf->filtro($sql);
		$objeProf->cerrarConexion();
		return $datos;
	}
	function ComboBoxTurno(){
		$sql = "SELECT `idturno`, `turno` FROM `turno`";
		$objeProf = new Datos();
		$datos = $objeProf->filtro($sql);
		$objeProf->cerrarConexion();
		return $datos;
	}
	function ComboBoxGrado(){
		$sql = "SELECT * FROM grado";
		$objeProf = new Datos();
		$datos = $objeProf->filtro($sql);
		$objeProf->cerrarConexion();
		return $datos;
	}
	function ComboBoxSeccion(){
		$sql = "SELECT `idseccion`, `seccion` FROM `seccion`";
		$objeProf = new Datos();
		$datos = $objeProf->filtro($sql);
		$objeProf->cerrarConexion();
		return $datos;
	}
        function mostrarGrado($idgrado){
                $sql = "SELECT grado FROM grado where idgrado='$idgrado'";
		
                $obj= new Datos();
		$datos = $obj->filtro($sql);
		$arreglo = $obj->proximo($datos);
		$obj->cerrarFiltro($datos);
		$obj->cerrarConexion();
		return $arreglo;
                
        }
        function mostrarSeccion($idseccion){
                $sql = "SELECT seccion FROM seccion where idseccion='$idseccion'";
		$obj= new Datos();
		$datos = $obj->filtro($sql);
		$arreglo = $obj->proximo($datos);
		$obj->cerrarFiltro($datos);
		$obj->cerrarConexion();
		return $arreglo;
        }
        function mostrarTurno($idturno){
                $sql = "SELECT turno FROM turno where idturno='$idturno'";
		$obj= new Datos();
		$datos = $obj->filtro($sql);
		$arreglo = $obj->proximo($datos);
		$obj->cerrarFiltro($datos);
		$obj->cerrarConexion();
		return $arreglo;
        }
}
?>