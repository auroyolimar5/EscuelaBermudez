<?php
extract($_REQUEST);
if (isset($_GET['mensaje'])){
	$mensaje = $_GET['mensaje'];
	}else{
		$mensaje = "";
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Index</title>
<script src="vista/lib/jquery-1.9.1.js" type="text/javascript"></script>
<script src="vista/lib/bootstrap-3.2.0/js/bootstrap.js" type="text/javascript"></script>
<script src="vista/js/principal.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="vista/lib/bootstrap-3.2.0/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="vista/css/css.css"/>
</head>

<body>
<!-- incio contenedor del encabezado -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- incio encabezado -->
			<div class="header1">
                <img src="vista/img/Banner MPPPE.jpg" align="left" class="img-responsive" alt="Responsive image"/>
			</div>
			<!-- fin encabezado -->
            <!--<h3>SIRESJ</h3>-->
		</div>
	</div>
</div>
<!-- fin contenedor del encabezado -->
<!-- separación -->
<hr id="separacion"/>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <!-- incio cuerpo -->
            <!-- título -->
             <div align="center"> <font color="#CC0000"red size=6 face="aharoni" align="center"><b>Sistema de Inscripción Unidad Educativa Distrital Bermúdez</b></font>.</div><br>
                        <!-- título -->
			<!-- incio cuerpo -->
            <!-- título -->
            <!-- título -->
			<div class="row">
				<div class="loguin">
					<div class="col-md-4 col-md-offset-4">
                    <!-- incio formulario usuario -->
					    <div id="login" class="login">
                           <h4 align="center"> <font color="#CC0000" red> <strong>Datos del Usuario</strong></font></h4>
                            <form id="form1" name="ingresarSistema" method="post" action="controladores/usuarios/iniSesion.php">
                            <div id="datosUsuario">
                               <p>
                                  <div class = "form-group has-feedback  has-feedback-ri">
                                    <label  class = "control-label" >Usuario:</label> 
                                    <input  type = "text" name="usuario" id="usuario" maxlength="30" class = "form-control"  placeholder = "Usuario" required autofocus/>
                                    <span class = "form-control-feedback glyphicon glyphicon-user"></span> 
                                  </div>
                                  <div  class = "form-group has-feedback">
                                    <label  class = "control-label">Clave:</label> 
                                    <input  type = "password" name="clave" id="clave" maxlength="20" class = "form-control"  placeholder = "Max. 20 caracteres" required/> 
                                    <input  type="hidden" name="operacion" value="1"/>
                                    <span class = "form-control-feedback glyphicon glyphicon-lock"></span> 
                                  </div>
                                  <p>
                                    <a href="#" onclick="recuperarUsuaio()">
                                      ¿Olvido su Usuario? 
                                      <span class="glyphicon glyphicon-hand-left"></span>
                                    </a>
                                  </p>
                                
                                  <div>
                                  	<input type="submit" accesskey="l" id="btn" class="btn btn-primary" value="Ingresar"/>
                                  </div>   
                                </p>
                            </div>
                           	</form>
                            <!-- fin formulario usuario -->
                            <!-- Inicio formulario email -->
                            <div id="datosEmail">
                            <form id="form2" name="ingresarSistema" method="post" action="controladores/usuarios/iniSesion.php">
                             <p>
                                  <div class = "form-group has-feedback  has-feedback-ri">
                                    <label  class = "control-label" >E-mail:</label> 
                                    <input  type = "text" name="email" id="email" maxlength="60" class = "form-control"  placeholder = "E-mail" maxlength="20" required /> 
                                  </div>
                                  <div  class = "form-group has-feedback">
                                    <label  class = "control-label">usuario:</label> 
                                    <input  type = "text" name="usuario" id="usuario" maxlength="20" class = "form-control"  placeholder = "Usuario" maxlength="10" required/>
                                    <input name="operacion" type="hidden" value="2" />
                                  </div>
                                  <p>
                                    <a href="#" onclick="volverLoguin()">
                                      Volver 
                                      <span class="glyphicon glyphicon-hand-left"></span>
                                    </a>
                                  </p>
                                  <div>
                                  	<input type="submit" accesskey="l" id="btn" class="btn btn-primary" value="Enviar"/>
                                  </div>   
                                </p>
                            </div>
                            </form>
                            <!-- fin formulario email -->
                            <!-- Inicio formulario registro -->
                            <div id="formReg">
                            	<form  id="form1" name="form1" method="post" action="controladores/usuarios/iniSesion.php">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">  
                                        <label>Nombre</label>
                                        <input type="text" name="primerNombre" class="form-control" placeholder="Nombre" maxlength="20" required></input>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label>Apellido</label>
                                        <input type="text" name="primerApellido" class="form-control" placeholder="Apellido" maxlength="20" required></input>
                                      </div>
                                    </div>
                                  </div>
                    							<div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>N° Cédula</label>
                                          <input type="text" name="cedula" class="form-control" placeholder="Número de Cédula" maxlength="10" required></input>
                                        </div>
                                      </div>	
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Nacionalidad</label>
                                          <select name="nacionalidad" id="" class="form-control" required>
                                             <option value="" selected="selected">Seleccione</option>
                                              <option value="V">Venezolano</option>
                                              <option value="E">Extranjero</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                    								<div class="form-group has-feedback has-feedback-ri">
                    									<label>E-mail</label>
                    									<input type="email" name="email" class="form-control" placeholder="tu_email@dominio.com" maxlength="50" required>
                    									<span class = "form-control-feedback glyphicon glyphicon-envelope"></span> 
                    								</div>
                    								<div class="form-group has-feedback has-feedback-ri">
                    									<label>Usuario</label>
                    									<input type="text" name="usuario" class="form-control" required maxlength="20"></input>
                    									<span class = "form-control-feedback glyphicon glyphicon-user"></span> 
                    								</div>
                    								<div class="form-group has-feedback  has-feedback-ri">
                    									<label>Password</label>
                    									<input type="password" name="clave" class="form-control" required maxlength="20"></input>
                    									<span class = "form-control-feedback glyphicon glyphicon-lock"></span>
                    								</div>
                                   	<input name="operacion" type="hidden" value="3"/>
                                <a href="#" onclick="ocultarFormReg()">
                                      Volver 
                                      <span class="glyphicon glyphicon-hand-left"></span>
                                    </a>
                                <div align="center">
                                    <input name="Restablecer" type="reset" class="btn btn-primary" value="Limpiar">
                                    <input type="submit" class="btn btn-primary" value="Enviar">
								</div>
              					</form>
							</div>
                            <!-- fin formulario registro -->
                        </div>
					</div>
				</div>
			</div>
		</div>
			<div class="row">
				
			</div>
			<!-- fin cuerpo -->
	</div>
</div>
</div>
<?php 
if (!empty($mensaje)){
	if($mensaje==1){echo"<script type='text/javascript'>alert('El Usuario o Contraseña no coninciden intenetelo de nuevo.');</script>";}
	if($mensaje==2){echo"<script type='text/javascript'>alert('El Usuario se encuentra bloqueado comuniquese con el administrador');</script>";}
	if($mensaje==3){echo"<script type='text/javascript'>alert('La cuenta E-mail no esta registrada en el sistema');</script>";}
	if($mensaje==4){echo"<script type='text/javascript'>alert('Los datos de usuario y clave fueron enviados a su cuenta de correo');</script>";}
	if($mensaje==5){echo"<script type='text/javascript'>alert('El usuario fue registrado exitosamente');</script>";}
	if($mensaje==6){echo"<script type='text/javascript'>alert('El Usuario ya existe intente con otro nombre de usuario');</script>";}
	if($mensaje==7){echo"<script type='text/javascript'>alert('Introduzca su Usuario y Clave para entrar en el sistema');</script>";}
}
?>
</body>
</html>