<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Constancia de Inscripción</title>
	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
	<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
	<script>
	function redireccion(){
		window.location='../gestionEstudiantes/inscripcion.php'
	}
	</script>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<button class="btn btn btn-primary" value="volver" onClick="redireccion()"><span class="glyphicon glyphicon-chevron-left"></span></span>Volver</button>
		</div>
		</br>
		<div><iframe src="../../lib/tcpdf/documentos/constanciaInscripcion.php" name="constancia"scrolling="No" frameborder="0" id="pdf"></iframe></div>
	</div>
</div>
</body>
</html>