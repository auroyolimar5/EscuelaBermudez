
--
-- Estrutura da tabela 'enfermedad'
--

DROP TABLE enfermedad CASCADE;
CREATE TABLE enfermedad (
enfermedad varchar(50),
idenfermedad int4 NOT NULL
);

--
-- Creating data for 'enfermedad'
--

INSERT INTO enfermedad VALUES ('1','Alergia');
INSERT INTO enfermedad VALUES ('2','Diabetes');
INSERT INTO enfermedad VALUES ('3','Hepatitis');
INSERT INTO enfermedad VALUES ('4','Hipertension');
INSERT INTO enfermedad VALUES ('5','Hipotiroidismo');
INSERT INTO enfermedad VALUES ('6','Sida');


--
-- Creating index for 'enfermedad'
--

ALTER TABLE ONLY  enfermedad  ADD CONSTRAINT  enfermedad_pkey  PRIMARY KEY  (idenfermedad);

--
-- Estrutura da tabela 'estudiante'
--

DROP TABLE estudiante CASCADE;
CREATE TABLE estudiante (
seccion_herm varchar(20),
telf_hab_est varchar(20) NOT NULL,
idturno int4 NOT NULL,
idgrado int4 NOT NULL,
grado_herm varchar(10),
nac_est varchar(10) NOT NULL,
foto varchar(100),
idseccion int4 NOT NULL,
sexo_est varchar(15) NOT NULL,
lugar_nac_est varchar(30) NOT NULL,
direccion varchar(150) NOT NULL,
id int4 NOT NULL DEFAULT nextval('estudiante_id_seq'::regclass),
periodo varchar(40),
repitiente varchar(2),
fec_nac_est varchar(20),
estatus_est varchar(15),
enfermedad varchar(2) NOT NULL,
s_nombre_est varchar(50) NOT NULL,
s_apellido_est varchar(50) NOT NULL,
p_apellido_est varchar(50) NOT NULL,
ci_est int4,
des_enfermedad varchar(100),
hermanos varchar(2) NOT NULL,
telf_cel_est varchar(20),
p_nombre_est varchar(50) NOT NULL
);

--
-- Creating data for 'estudiante'
--

INSERT INTO estudiante VALUES ('2','Pico','Jara','Luis','Miguel','Venezolano','2116087','2','2','1','2014-2015','No','M','1994-03-12','caracas','la vega','02126998631','04169145509','No',NULL,'Si','1','2','../../img/fotos/2/2/fulanito.jpg','activo');
INSERT INTO estudiante VALUES ('1','pico','jara','pedro','jose','Venezolano','19852337','1','1','1','2014-2015','No','M','1992-02-25','caracas','la vega','02126998631','04169145509','No',NULL,'Si','3','2','../../img/fotos/1/1/einstein.jpg','retirado');
INSERT INTO estudiante VALUES ('3','SULBARAN','BRICEO','EILYN','VALENTINA','Venezolano','1652488411','1','1','1','2015-2016','No','F','2009-11-11','CARACAS','LA VEGA','02124765306','04122205306','No',NULL,'No','1','1','../../img/fotos/1/1/584262130005.jpg','activo');


--
-- Creating index for 'estudiante'
--

ALTER TABLE ONLY  estudiante  ADD CONSTRAINT  estudiante_pkey  PRIMARY KEY  (id);

--
-- Estrutura da tabela 'grado'
--

DROP TABLE grado CASCADE;
CREATE TABLE grado (
grado varchar(5) NOT NULL,
idgrado int4 NOT NULL
);

--
-- Creating data for 'grado'
--

INSERT INTO grado VALUES ('1','1er');
INSERT INTO grado VALUES ('2','2do');
INSERT INTO grado VALUES ('3','3er');
INSERT INTO grado VALUES ('4','4to');
INSERT INTO grado VALUES ('5','5to');
INSERT INTO grado VALUES ('6','6to');


--
-- Creating index for 'grado'
--

ALTER TABLE ONLY  grado  ADD CONSTRAINT  grado_pkey  PRIMARY KEY  (idgrado);

--
-- Estrutura da tabela 'madre'
--

DROP TABLE madre CASCADE;
CREATE TABLE madre (
email varchar(60),
cedula_madre int4 NOT NULL,
p_apellido_madre varchar(50) NOT NULL,
s_nombre_madre varchar(50) NOT NULL,
direccion_madre varchar(150),
s_apellido_madre varchar(50) NOT NULL,
vive_con_est varchar(2),
p_nombre_madre varchar(50) NOT NULL,
lugar_trab_madre varchar(50),
telf_cel_madre varchar(30),
id_prof varchar(20),
nac_madre varchar(20) NOT NULL,
telf_hab_madre varchar(30),
id_est int4 NOT NULL,
telf_ofi_madre varchar(30)
);

--
-- Creating data for 'madre'
--



--
-- Creating index for 'madre'
--

ALTER TABLE ONLY  madre  ADD CONSTRAINT  madre_pkey  PRIMARY KEY  (id_est);

--
-- Estrutura da tabela 'padre'
--

DROP TABLE padre CASCADE;
CREATE TABLE padre (
telf_cel_padre varchar(30),
telf_ofi_padre varchar(30),
email varchar(60),
nac_padre varchar(20) NOT NULL,
s_apellido_padre varchar(50) NOT NULL,
lugar_trab_padre varchar(50),
direccion_padre varchar(150),
vive_con_est varchar(2),
cedula_padre int4 NOT NULL,
telf_hab_padre varchar(30),
p_nombre_padre varchar(50) NOT NULL,
p_apellido_padre varchar(50) NOT NULL,
s_nombre_padre varchar(50) NOT NULL,
id_prof varchar(20),
id_est int4 NOT NULL
);

--
-- Creating data for 'padre'
--



--
-- Creating index for 'padre'
--

ALTER TABLE ONLY  padre  ADD CONSTRAINT  padre_pkey  PRIMARY KEY  (id_est);

--
-- Estrutura da tabela 'profecionoficio'
--

DROP TABLE profecionoficio CASCADE;
CREATE TABLE profecionoficio (
idprofecionoficio int4 NOT NULL,
profecionoficio varchar(45)
);

--
-- Creating data for 'profecionoficio'
--

INSERT INTO profecionoficio VALUES ('1','Administrador(a)');
INSERT INTO profecionoficio VALUES ('2','Alba�il');
INSERT INTO profecionoficio VALUES ('3','Ama de Casa');
INSERT INTO profecionoficio VALUES ('4','Analista(a)');
INSERT INTO profecionoficio VALUES ('5','Carpintero(a)');
INSERT INTO profecionoficio VALUES ('6','Comerciante');
INSERT INTO profecionoficio VALUES ('7','Contador(a)');
INSERT INTO profecionoficio VALUES ('8','Enfermero(a)');
INSERT INTO profecionoficio VALUES ('9','Ingeniero(a) Civil');
INSERT INTO profecionoficio VALUES ('10','Maestro(a)');
INSERT INTO profecionoficio VALUES ('11','Medico');
INSERT INTO profecionoficio VALUES ('12','M�sico');
INSERT INTO profecionoficio VALUES ('13','Obrero(a)');
INSERT INTO profecionoficio VALUES ('14','Pintor(a)');
INSERT INTO profecionoficio VALUES ('15','Policia');
INSERT INTO profecionoficio VALUES ('16','Profesor(a)');
INSERT INTO profecionoficio VALUES ('17','Tec.Informatico');
INSERT INTO profecionoficio VALUES ('18','Vigilante');
INSERT INTO profecionoficio VALUES ('19','Otra');


--
-- Creating index for 'profecionoficio'
--

ALTER TABLE ONLY  profecionoficio  ADD CONSTRAINT  profecionoficio_pkey  PRIMARY KEY  (idprofecionoficio);

--
-- Estrutura da tabela 'profesores'
--

DROP TABLE profesores CASCADE;
CREATE TABLE profesores (
foto_prof varchar(150) NOT NULL,
s_nombre_prof varchar(50) NOT NULL,
lugar_nac_prof varchar(100) NOT NULL,
idseccion_prof int4 NOT NULL,
telf_hab_prof varchar(15) NOT NULL,
fecha_nac_prof varchar(11) NOT NULL,
telf_cel_prof varchar(15) NOT NULL,
sexo_prof varchar(15) NOT NULL,
p_nombre_prof varchar(50) NOT NULL,
email_prof varchar(70) NOT NULL,
idgrado_prof int4 NOT NULL,
cedula_prof int4 NOT NULL,
p_apellido_prof varchar(50) NOT NULL,
idturno_prof int4 NOT NULL,
estatus_prof varchar(15),
nac_profesor varchar(20) NOT NULL,
s_apellido_prof varchar(50) NOT NULL,
direccion_prof varchar(150) NOT NULL,
id int4 NOT NULL DEFAULT nextval('profesores_id_seq'::regclass)
);

--
-- Creating data for 'profesores'
--

INSERT INTO profesores VALUES ('1','V','12345678','Jara','H','Ruben','Patricio','M','1985-07-31','Caracas','Carapita - Antimano','02126998631','04169145509','1','1','1','rubenj2607@gmail.com','../../img/docentes/1/1/1/einstein.jpg','activo');
INSERT INTO profesores VALUES ('2','V','8654321','Pico','Jara','Judith','Imelda','F','1985-07-03','Caracas','La Castellana - Chacao','02126998631','04169145509','2','1','1','judithp88@gmail.com','../../img/docentes/2/1/1/pedropico.png','activo');
INSERT INTO profesores VALUES ('3','V','21116087','Pico','Jara','Luis','Miguel','M','1994-03-12','Caracas','La vega - sector las torres','02126998631','04169145509','1','2','1','luispicojara_94@hotmail.com','../../img/docentes/1/2/1/pedropico.png','retirado');


--
-- Creating index for 'profesores'
--

ALTER TABLE ONLY  profesores  ADD CONSTRAINT  profesores_pkey  PRIMARY KEY  (cedula_prof);

--
-- Estrutura da tabela 'representante'
--

DROP TABLE representante CASCADE;
CREATE TABLE representante (
id_est int4 NOT NULL DEFAULT nextval('representante_id_est_seq'::regclass),
s_nombre_repres varchar(50) NOT NULL,
nac_repres varchar(20) NOT NULL,
telf_ofi_repres varchar(30),
parentesco varchar(50) NOT NULL,
p_nombre_repres varchar(50) NOT NULL,
vive_con_est varchar(2),
email varchar(60),
s_apellido_repres varchar(50) NOT NULL,
lugar_trab_repres varchar(50),
cedula_repres int4 NOT NULL,
direccion_repres varchar(150),
p_apellido_repres varchar(50) NOT NULL,
telf_cel_repres varchar(30),
telf_hab_repres varchar(30),
ocupacion_rep int4 NOT NULL
);

--
-- Creating data for 'representante'
--

INSERT INTO representante VALUES ('1','jara','marcatoma','rosa','imelda','Venezolano(a)','21529199','Madre','3','caracas','la vega sector las torres','Si','02126998631','04169146527','02126009111','rosaj65@gmail.com');
INSERT INTO representante VALUES ('2','Pico','Barci','Pedro','E','Venezolano(a)','22912246','Padre','2','caracas','la vega sector las torres','Si','02126998631','04169146527','02126009111','pedropicob@gmail.com');
INSERT INTO representante VALUES ('3','BRICEO','MEJIA','ELEINY','DELCARMEN','Venezolano(a)','16524884','Madre','17','CHACAO','LA VEGA','Si','02124765306','04122205306',NULL,'eleiny_18@hotmail.com');


--
-- Creating index for 'representante'
--

ALTER TABLE ONLY  representante  ADD CONSTRAINT  representante_pkey  PRIMARY KEY  (id_est);

--
-- Estrutura da tabela 'seccion'
--

DROP TABLE seccion CASCADE;
CREATE TABLE seccion (
idseccion int4 NOT NULL,
seccion varchar(10) NOT NULL
);

--
-- Creating data for 'seccion'
--

INSERT INTO seccion VALUES ('1','A - 1');
INSERT INTO seccion VALUES ('2','A - 2');
INSERT INTO seccion VALUES ('3','A - 3');
INSERT INTO seccion VALUES ('4','B - 1');
INSERT INTO seccion VALUES ('5','B - 2');
INSERT INTO seccion VALUES ('6','B - 3');


--
-- Creating index for 'seccion'
--

ALTER TABLE ONLY  seccion  ADD CONSTRAINT  seccion_pkey  PRIMARY KEY  (idseccion);

--
-- Estrutura da tabela 'sexo'
--

DROP TABLE sexo CASCADE;
CREATE TABLE sexo (
sexo varchar(20),
idsexo int4 NOT NULL
);

--
-- Creating data for 'sexo'
--

INSERT INTO sexo VALUES ('1','Masculino');
INSERT INTO sexo VALUES ('2','Femenino');

--
-- Estrutura da tabela 'turno'
--

DROP TABLE turno CASCADE;
CREATE TABLE turno (
turno varchar(50),
idturno int4 NOT NULL
);

--
-- Creating data for 'turno'
--

INSERT INTO turno VALUES ('1','Ma�ana');
INSERT INTO turno VALUES ('2','Tarde');


--
-- Creating index for 'turno'
--

ALTER TABLE ONLY  turno  ADD CONSTRAINT  turno_pkey  PRIMARY KEY  (idturno);

--
-- Estrutura da tabela 'usuarios'
--

DROP TABLE usuarios CASCADE;
CREATE TABLE usuarios (
nombre varchar(50) NOT NULL,
apellido varchar(50) NOT NULL,
email varchar(70) NOT NULL,
categoria varchar(30) NOT NULL,
cedula int4 NOT NULL,
fechareg varchar(10) NOT NULL,
clave varchar(70) NOT NULL,
usuario varchar(50) NOT NULL,
id int4 NOT NULL DEFAULT nextval('usuarios_id_seq'::regclass),
estatus varchar(30) NOT NULL
);

--
-- Creating data for 'usuarios'
--

INSERT INTO usuarios VALUES ('1','pedrop2502','Pedro','Pico','19852338','pedrop2502@gmail.com','123456','Super-Admin','activo','22-11-2015');
INSERT INTO usuarios VALUES ('2','luisp','Luis','Pico','21116087','luispicojara_94@hotmail.com','123456','Admin','retirado','22-11-2015');


--
-- Creating index for 'usuarios'
--

ALTER TABLE ONLY  usuarios  ADD CONSTRAINT  usuarios_pkey  PRIMARY KEY  (usuario);
