﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<?php 
$usuario=$_SESSION['usuario'];
$identificacion=$_SESSION['identificacion'];
?>

        <div style="text-align: right"> <?php echo 'Bienvenido '.$identificacion; ?></div>

    
    <?php
  if($_SESSION['categoria']=='Admin'){
    echo 
    '<div class="row">
  <div class="col-sm-12">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
                 
          <ul class="nav navbar-nav">
          
          <li class="dropdown">
          <a href="javascript:document.location.reload();">INICIO</a>
          </li>
          
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Usuarios<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
              <a href="gestionUsuarios/crearUsuario.php" target="contenido">
                <span class="glyphicon glyphicon-plus"></span> Ingresar
              </a>
            </li>
             <li><a href="gestionUsuarios/listaUsuarios.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Editar/Eliminar Usuario</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Estudiante <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionEstudiantes/inscripcion.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Inscripción </a></li>
            <li ><a href="gestionEstudiantes/listaEstudiantes.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Vista/Editar/Eliminar </a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Notas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionNotas/consulta_notas.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Consulta de Notas </a></li>
            <li ><a href="gestionNotas/carga_notas.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Cargar Notas </a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Docente <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="gestionDocentes/incluirDocentes.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Ingresar</a></li>
            <li><a href="gestionDocentes/listaDocentes.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Editar/Eliminar</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Consultas<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li role="presentation"><a href="gestionEstudiantes/fichaEstudiante.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Estudiante</a></li>
            <li role="presentation"><a href="gestionDocentes/fichaDocente.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Docente</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Reportes <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaEstudios.php" target="contenido"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Estudio </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaInscripcion2.php" target="contenido"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Inscripción </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaRetiro.php" target="contenido"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Retiro </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaBuenaConducta.php" target="contenido"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Buena Conducta </b></a>
           <li ><a class="dropdown-toggle" href="../lib/tcpdf/documentos/Estadisticas.php" target="contenido"><span class="glyphicon glyphicon-stats"></span> Estadísticas </a>
          </ul>
        </li>
        <li class="dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" href="#">Mantenimiento <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="../../vista/paginas/backupc.php"><span class="glyphicon glyphicon-floppy-save"></span> Respaldo </a>
          </ul>
        </li>
        <li class="dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" href="#"> Ayuda<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="ayuda/ManualUsuario.pdf"><span class="glyphicon glyphicon-book"></span> Manual de Usuario </a>
            <li ><a class="dropdown-toggle" href="ayuda/contactanos2.pdf"><span class="glyphicon glyphicon-envelope"></span> Contactanos </a>
          </ul>
        </li>
      </ul>
      </ul>
       <ul class="nav navbar-nav navbar-right">
          <li class="dropdown"><a class="dropdown-toggle" href="../../controladores/usuarios/cerraSession.php"><span class="glyphicon glyphicon-log-out" style="font-size:20px;" title="Salir"></span></a></li> 
    </div>
  </nav>';
  }
 
  if($_SESSION['categoria']=="Profesor"){
    echo 
    '<div class="row">
  <div class="col-sm-12">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
        <ul class="nav navbar-nav">
        
        <li class="dropdown">
          <a href="javascript:document.location.reload();">INICIO</a>
          </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Estudiante <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionEstudiantes/inscripcion.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Inscripción </a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Notas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionNotas/consulta_notas.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Consulta de Notas </a></li>
            <li ><a href="gestionNotas/carga_notas.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Cargar Notas </a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Docente <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="gestionDocentes/incluirDocentes.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Ingresar</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Consultas<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li role="presentation"><a href="gestionEstudiantes/fichaEstudiante.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Estudiante</a></li>
            <li role="presentation"><a href="gestionDocentes/fichaDocente.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Docente</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Reportes <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaEstudios.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Estudio </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaInscripcion2.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Inscripción </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaRetiro.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Retiro </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaBuenaConducta.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Buena Conducta </b></a>
           <!--  <li ><a class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-stats"></span> Estadísticas </a> -->
          </ul>
        </li>
        <li class="dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" href="#"> Ayuda<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="ayuda/ManualUsuario.pdf"><span class="glyphicon glyphicon-book"></span> Manual de Usuario </a>
            <li ><a class="dropdown-toggle" href="ayuda/contactanos2.pdf"><span class="glyphicon glyphicon-envelope"></span> Contactanos </a>
          </ul>
        </li>
      </ul>
      </ul>
       <ul class="nav navbar-nav navbar-right">
          <li class="dropdown"><a class="dropdown-toggle" href="../../controladores/usuarios/cerraSession.php"><span class="glyphicon glyphicon-log-out" style="font-size:20px;" title="Salir"></span></a></li> 
    </div>
  </nav>';
  }
  if($_SESSION['categoria']=="Secretaria"){
    echo 
    '<div class="row">
  <div class="col-sm-12">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav">
      
      <li class="dropdown">
          <a href="javascript:document.location.reload();">INICIO</a>
          </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Estudiante <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionEstudiantes/inscripcion.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Inscripción </a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Docente <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <!-- <li><a href="gestionDocentes/incluirDocentes.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Ingresar</a></li> -->
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Consultas<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li role="presentation"><a href="gestionEstudiantes/fichaEstudiante.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Estudiante</a></li>
            <li role="presentation"><a href="gestionDocentes/fichaDocente.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Docente</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Reportes <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaEstudios.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Estudio </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaInscripcion2.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Inscripción </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaRetiro.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Retiro </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaBuenaConducta.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Buena Conducta </b></a>
           <!--  <li ><a class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-stats"></span> Estadísticas </a> -->
          </ul>
        </li>
        <li class="dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" href="#"> Ayuda<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="ayuda/ManualUsuario.pdf"><span class="glyphicon glyphicon-book"></span> Manual de Usuario </a>
            <li ><a class="dropdown-toggle" href="ayuda/contactanos2.pdf"><span class="glyphicon glyphicon-envelope"></span> Contactanos </a>
          </ul>
        </li>
      </ul>
      </ul>
       <ul class="nav navbar-nav navbar-right">
          <li class="dropdown"><a class="dropdown-toggle" href="../../controladores/usuarios/cerraSession.php"><span class="glyphicon glyphicon-log-out" style="font-size:20px;" title="Salir"></span></a></li> 
    </div>
  </nav>';
  }
  
  if($_SESSION['categoria']=="Director"){
    echo 
    '<div class="row">
  <div class="col-sm-12">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav">
      
      <li class="dropdown">
          <a href="javascript:document.location.reload();">INICIO</a>
          </li>

        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Usuarios<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
              <a href="gestionUsuarios/crearUsuario.php" target="contenido">
                <span class="glyphicon glyphicon-plus"></span> Ingresar
              </a>
            </li>
             <li><a href="gestionUsuarios/listaUsuarios.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Editar/Eliminar Usuario</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Estudiante <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionEstudiantes/inscripcion.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Inscripción </a></li>
            <li ><a href="gestionEstudiantes/listaEstudiantes.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Vista/Editar/Eliminar </a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Notas <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a href="gestionNotas/consulta_notas.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Consulta de Notas </a></li>
            </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Docente <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="gestionDocentes/incluirDocentes.php" target="contenido"><span class="glyphicon glyphicon-plus"></span> Ingresar</a></li>
            <li><a href="gestionDocentes/listaDocentes.php" target="contenido"><span class="glyphicon glyphicon-pencil"></span> Editar/Eliminar</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Consultas<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li role="presentation"><a href="gestionEstudiantes/fichaEstudiante.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Estudiante</a></li>
            <li role="presentation"><a href="gestionDocentes/fichaDocente.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Docente</a></li>
          </ul>
        </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Reportes <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaEstudios.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Estudio </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaInscripcion2.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Inscripción </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaRetiro.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Retiro </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaBuenaConducta.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Buena Conducta </b></a>
            <li ><a class="dropdown-toggle" href="../lib/tcpdf/documentos/Estadisticas.php"><span class="glyphicon glyphicon-stats"></span> Estadísticas </a> 
          </ul>
        </li>
        <li class="dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" href="#"> Ayuda<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="ayuda/ManualUsuario.pdf"><span class="glyphicon glyphicon-book"></span> Manual de Usuario </a>
            <li ><a class="dropdown-toggle" href="ayuda/contactanos2.pdf"><span class="glyphicon glyphicon-envelope"></span> Contactanos </a>
          </ul>
        </li>
      </ul>
      </ul>
       <ul class="nav navbar-nav navbar-right">
          <li class="dropdown"><a class="dropdown-toggle" href="../../controladores/usuarios/cerraSession.php"><span class="glyphicon glyphicon-log-out" style="font-size:20px;" title="Salir"></span></a></li> 
    </div>
  </nav>';
  }
  if($_SESSION['categoria']=="Usuario"){
    echo 
    '<div class="row">
  <div class="col-sm-12">
    <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav">
      
      <li class="dropdown">
          <a href="javascript:document.location.reload();">INICIO</a>
          </li> 
       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Consultas<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li role="presentation"><a href="gestionEstudiantes/fichaEstudiante.php" target="contenido"><span class="glyphicon glyphicon-tag"></span> Ficha de Estudiante</a></li>
          </ul>
          </li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Reportes <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaEstudios.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Estudio </a>
            <li ><a class="dropdown-toggle" href="getionReportes/constanciaInscripcion2.php"><span class="glyphicon glyphicon-list-alt"></span> Constancia de Inscripción </a>
            <li ><a class="dropdown-toggle" href="../lib/tcpdf/documentos/Estadisticas.php" target="contenido"><span class="glyphicon glyphicon-stats"></span> Estadísticas </a>
          </ul>
        </li>
        <li class="dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" href="#"> Institución<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li ><a class="dropdown-toggle" href="../img/pdf/ReseñaHistorica.pdf" target="contenido"><span class="glyphicon glyphicon-book"></span> Reseña Histórica </a>
          </ul>
        </li>
      </ul>
      </ul>
       <ul class="nav navbar-nav navbar-right">
          <li class="dropdown"><a class="dropdown-toggle" href="../../controladores/usuarios/cerraSession.php"><span class="glyphicon glyphicon-log-out" style="font-size:20px;" title="Salir"></span></a></li> 
    </div>
  </nav>';
  }
?>
</body>
</html>
