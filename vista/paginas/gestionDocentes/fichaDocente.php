<?php 
extract($_REQUEST);
if (isset($mensaje)) {
	$mensaje = $_GET['mensaje'];
}else{
	$mensaje = '';
}
?>
<!DOCTYPE html>
<html lang="es"><head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
    <script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
    <script language="javascript" src="../../js/principal.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 align="left">Consultas/Ficha del Docente</h4>
			<br>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<form action="../../../controladores/gestionDocentes/con_fichaDocente.php" method="post" name="form1">
			<div class="col-md-3">
				<div class="form-group">
					<input type="text" name="cedula_prof" class="form-control numeric" placeholder="Cédula del Docente" required>
					<input type="hidden" name="accion" value="1">
				</div>
			</div>
			<div class="col-md-3"><input type="submit" class="btn btn-primary" value="Buscar"></div>
		</form>
	</div>
</div>
<div class="container" id="form">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<div class="col-md-12">
						<label for="">Foto</label>
						<div align="center"><img src="<?php if(isset($foto)){echo $foto;} ?>" width="208" height="275"> </div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="col-md-12"><h4>Datos del Docente</h4></div>
					<div class="col-md-6">
						<!-- <label for="">Columna 1</label> -->
						<div class="form-group"><label for="">Nombres</label><input type="text" class="form-control" value="<?php if(isset($primerNombre)){echo $primerNombre.' '.$segundoNombre;} ?>"></div>
						<div class="form-group"><label for="">Apellidos</label><input type="text" class="form-control" value="<?php if(isset($primerApellido)){echo $primerApellido.' '.$segundoApellido;} ?>"></div>
						<div class="form-group"><label for="">Cédula</label><input type="text" class="form-control" value="<?php if(isset($cedula)){echo $cedula;} ?>"></div>
					</div>
					<div class="col-md-6">
						<!-- <label for="">Columna 2</label> -->
						<div class="col-md-6">
							<div class="form-group"><label for="">Grado</label><input class="form-control" value="<?php if(isset($grado)){echo $grado;} ?>"></input></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Sección</label><input type="text" class="form-control" value="<?php if(isset($seccion)){echo $seccion;} ?>"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Turno</label><input type="text" class="form-control" value="<?php if(isset($turno)){echo $turno;} ?>"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Nacionalidad</label><input type="text" class="form-control" value="<?php if(isset($nacionalidad)){echo $nacionalidad;} ?>"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Fch. Nac.</label><input type="text" class="form-control" value="<?php if(isset($fechaNac)){echo $fechaNac;} ?>"></div>
						</div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label for="">Telf. Movil</label><input type="text" class="form-control" value="<?php if(isset($celular)){echo $celular;} ?>" style="width: 120px"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group"><label for="">Dirección</label><input type="text" class="form-control" value="<?php if(isset($direccion)){echo $direccion;} ?>"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<?php 
	if (!empty($mensaje)){
	if($mensaje==1){echo"<script type='text/javascript'> alert('Cédula de idenidad no resgistrada en el sistema')</script>";}
	if($mensaje==2){echo"<script type='text/javascript'> alert('Docente no regstrado en el sistema')</script>";}
	}
?>
