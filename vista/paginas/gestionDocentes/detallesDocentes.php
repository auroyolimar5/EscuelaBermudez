<?php 
extract($_REQUEST);
if(isset($_GET['id'])){
	$var2 = $_GET['id'];
	}else{
		$var2 = "";
		}
include_once("../../../controladores/gestionDocentes/con_detallesDocente.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 ">

<script type="text/javascript" src="../../lib/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
<title>Documento sin título</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12" >
			<h4 align="left">Docentes/Detalles Docente</h4>
			<br>
		</div>
	</div>
</div>
<div class="container" id="form">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<div class="col-md-12">
						<label for="">Foto</label>
						<div align="center"><img src="<?php echo $foto?>" width="208" height="275"></div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="col-md-12"><h4>Datos del Docente</h4></div>
					<div class="col-md-6">
						<!-- <label for="">Columna 1</label> -->
						<div class="form-group"><label for="">Nombres</label><span type="text" class="form-control"><?php echo $primerNombre." ".$segundoNombre?></span></div>
						<div class="form-group"><label for="">Apellidos</label><span type="text" class="form-control"><?php echo $primerApellido." ".$segundoApellido?></span></div>
						<div class="form-group"><label for="">Cédula</label><span type="text" class="form-control"><?php echo $cedula?></span></div>
					</div>
					<div class="col-md-6">
						<!-- <label for="">Columna 2</label> -->
						<div class="col-md-6">
							<div class="form-group"><label for="">Nacionalidad</label><span class="form-control"><?php echo $nacionalidad?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Fch. Nac.</label><span type="text" class="form-control"><?php echo $fechaNac?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Turno</label><span type="text" class="form-control"><?php echo $turno?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Grado</label><span type="hidden" class="form-control"><?php echo $grado?></span></div>
						</div>
						<div class="col-md-3">
                                                    <div class="form-group"><label for="">Sección</label><span type="text" class="form-control" style="width: 60px"><?php echo $seccion?></span></div>
						</div>
						<div class="col-md-9">
							<div class="form-group"><label for="">e-mail</label><span type="text" class="form-control"><?php echo $email?></span></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group"><label for="">Dirección</label><span type="text" class="form-control"><?php echo $direccion ?></span></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div align="center">
							<a href="listaDocentes.php" class="btn btn-primary">Volver</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>