<?php 
extract($_REQUEST);
include_once('../../js/combosAjax.php');
if(isset($_GET['mensaje'])){
$mensaje = $_GET['mensaje'];
}
 ?>
 <!DOCTYPE html>
 <html lang="es">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
	<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
	<script language="javascript" src="../../lib/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="../../lib/jquery-ui-1.11.2.custom/jquery-ui.css">
	<script language="javascript" src="../../js/principal.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
	<script>
//función del calendario
$(function() {
$( "#datepicker" ).datepicker({ maxDate: "0D" });
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: 'Anterior',
		nextText: 'Siguiente',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
		dayNamesShort: ['Dom','Lun','Mar','Mie;','Juv','Vie','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sab'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		changeMonth: true,
		changeYear: true,
		yearRange: "1920:2014",
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
</script>
 </head>
 <body>
	 <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h4 align="left">Gestión Docentes/Ingresar</h4>
				<br>
			</div>
		</div>
	</div>
 	<div class="container">
 		<div class="row">
 			<div class="col-md-12">
 				<div></div>
 				<div id="form-estudiante">
					<div class="container" id="form">
						<div class="row">
							<form action="../../../controladores/gestionDocentes/con_registroDocentes.php" method="post" enctype="multipart/form-data">
								<div class="col-md-3 ">
									<label for="" class="label-control">Foto del Docente</label>
									<div class="form-group">
										<input type="file" class="btn btn-primary" name="archivo" id="archivo" value="Examinar" onclick="" required/>
										</br>
										<input type="submit" value="Subir Foto" />
									</div>
								</div>
								<div class="col-md-1 col-md-offset-2">
									<div class="form-group">
									<label for="" class="label-control">Grado</label>
									<select class="form-control" name="grado" id="grado" onfocus="" required >
									  <option value="">--Seleccionar--</option>
								  	</select>
                                  </div>
								</div>
								<div class="col-md-1">
									<div class="form-group">
										<label for="" class="label-control">Sección</label>
										<select class="form-control" name="seccion" id="seccion" required>
										  	<option value="">--Seleccionar--</option>
									  	</select>
								  	</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label for="" class="label-control">Turno</label>
										<select class="form-control" name="turno" id="turno" required>
										  	<option value="">--Seleccionar--</option>
									  	</select>
										<input type="hidden" name="accion" value="1" />	
								  	</div>
								</div>
								<div class="col-md-3">
								<div class="form-group">
									<label class="label-control">Imagen</label>
									<div id="imagen">
									<?php if(isset($ruta)){
										echo "<img src='".$ruta."' width='100' height='110' />";
										}
									?>
									</div>
								</div>
							</div>
							</form>
						</div>
						<form data-toggle="validator" action="../../../controladores/gestionDocentes/con_registroDocentes.php" method="post" name="form2">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group has-feedback">
									<label class="label-control">Primer Apellido</label>
									<input class="form-control text" name="p_apellido_prof" required><span class="erores"></span></input>
									
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group has-feedback">
									<label class="label-control">Segundo Apellido</label>
									<input class="form-control text" name="s_apellido_prof"></input>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group has-feedback">
									<label class="label-control">Primer Nombre</label>
									<input class="form-control text" name="p_nombre_prof" required></input>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group has-feedback">
									<label class="label-control">Segundo Nombre</label>
									<input class="form-control text" name="s_nombre_prof"></input>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group has-feedback">
									<label class="label-control">Nac.</label>
									<select class="form-control" name="nac_profesor" required>
									  <option value="V">Venezolano</option>
									  <option value="E">Extranjero</option>
									</select>
								</div>
							</div>
                                                    
                                                    
                                                        <div class="col-md-2">
								<div class="form-group has-feedback">
									<label class="label-control">C.I.</label>
									<input class="form-control numeric" name="cedula_prof" required></input>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group has-feedback">
									<label class="label-control">Sexo</label>
									<select class="form-control" name="sexo_prof" required>
									  <option value=""selected="selected">seleccione</option>
									  <option value="M">Masculino</option>
									  <option value="F">Femenino</option>
									</select>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group has-feedback">
                                    <label class="laberl-control">Fecha de Nac</label>
                                    <div class='input-group date' id="datetimepicker">
                                        <input type='text' class="form-control" id="datepicker" name="fecha_nac_prof" required/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
							</div>
							<div class="col-md-3">
								<div class="form-group has-feedback">
									<label class="label-control">Lugar de Nacimiento</label>
									<input class="form-control text" name="lugar_nac_prof" required></input>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<label class="label-control">Dirección de Habitación</label>
									<input class="form-control" name="direccion_prof" required></input>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group has-feedback">
								<label class="label-control">Telf Habitación</label>
									<div class="input-group">
										<input class="form-control" name="telf_hab_prof">
										<span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group has-feedback">
								<label class="label-control">Telf Celular</label>
									<div class="input-group">
										<input class="form-control" name="telf_cel_prof"></input>
										<span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
									</div>
								</div>
							</div>
<!--							<div class="col-md-3">
									<div class="form-group">
									<label for="" class="label-control">Grado</label>
									<select class="form-control" name="grado" id="grado2" required >
									  <option value="">--Seleccionar--</option>
								  	</select>
                                  </div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="" class="label-control">Sección</label>
										<select class="form-control" name="seccion" id="seccion2" required>
										  	<option value="">--Seleccionar--</option>
									  	</select>
								  	</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label for="" class="label-control">Turno</label>
										<select class="form-control" name="turno" id="turno2" required>
										  	<option value="">--Seleccionar--</option>
									  	</select>
								  	</div>
								</div>-->
							<div class="col-md-3">
								<div class="form-group">
									<label for="" class="label-control" >E-mail</label>
									<input type="text" name="email_prof" class="form-control" required/>
								</div>
							</div>
							<input type="hidden" name="accion" value="2" />
							<input type="hidden" name="foto_prof" value="<?php if(isset($ruta)){echo $ruta;}?>" >
                                                        <input type="hidden" name="idgrado_prof" value="<?php if(isset($idgrado)){echo $idgrado;}?>" >
                                                        <input type="hidden" name="idseccion_prof" value="<?php if(isset($idseccion)){echo $idseccion;}?>" >
                                                        <input type="hidden" name="idturno_prof" value="<?php if(isset($idturno)){echo $idturno;}?>" >
						</div>
						<div class="row">
							<div class="col-md-12">
								<div align="center">
									<br>
									<input type="submit" value="Guardar" class="btn btn-primary"></intup>
								</div>
							</div>
						</div>
					</form>
				</div>
 			</div>
 		</div>
 	</div>
</body>
</html>
<?php 
if (!empty($mensaje)){
	if($mensaje==1){echo"<script type='text/javascript'> alert('El Docente ha sido registrado exitosamente')</script>";}
	if($mensaje==4){echo"<script type='text/javascript'>alert('El Regìsto no se ha realizado, El docente ya esta registrado en el sistema');</script>";}
}
 ?>