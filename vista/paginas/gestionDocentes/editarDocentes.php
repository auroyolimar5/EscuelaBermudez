<?php 
extract($_REQUEST);
include_once("../../../controladores/gestionDocentes/con_detallesDocente.php");
include_once('../../js/combosAjax.php');
?>
<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
	<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
	<script language="javascript" src="../../js/principal.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 align="left">Gestión de Docentes/Editar Docente</h4>
			<br>
		</div>
	</div>
</div>
<form action="../../../controladores/gestionDocentes/con_editarDocente.php" method="post" name="form1">
	<div class="container" id="form">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<div class="col-md-12">
							<label for="">Foto</label>
							<div align="center"><img src="<?php echo $foto?>" width="208" height="275"></div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="col-md-12"><h4>Datos del Docente</h4></div>
						<div class="col-md-6">
							<!-- <label for="">Columna 1</label> -->
							<div class="col-md-6">
								<div class="form-group">
									<label for="">1er Nombre</label>
									<input class="form-control text" name="primerNombre" value="<?php echo $primerNombre?>"></input>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">2do Nombre</label>
									<input class="form-control text" name="segundoNombre" value="<?php echo $segundoNombre?>"></input>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">1er Apellido</label>
									<input class="form-control text" name="primerApellido" value="<?php echo $primerApellido?>"></input>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">2do Apellidos</label>
									<input class="form-control text" name="segundoApellido" value="<?php echo $segundoApellido?>"></input>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="">Cédula</label>
									<input class="form-control numeric" name="cedula" value="<?php echo $cedula?>"></input>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- <label for="">Columna 2</label> -->
							<div class="col-md-6">
								<div class="form-group"><label for="">Nacionalidad</label><input class="form-control text" name="nacionalidad" value="<?php echo $nacionalidad  ?>"></input></div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="">Fch. Nac.</label><input type="text" class="form-control" name="fechaNac" value="<?php echo $fechaNac ?>"></input></div>
							</div>
							<div class="col-md-6">
									<div class="form-group">
										<label for="" class="label-control">Turno</label>
										<select class="form-control" name="turno" id="turno" required>
										  	<option value="<?php echo $idturno?>"><?php echo $turno?></option>
									  	</select>
										<input type="hidden" name="accion" value="1" />	
								  	</div>
								</div>
							<div class="col-md-3 ">
								<div class="form-group">
									<label for="" class="label-control">Grado</label>
                                                                        <select class="form-control" name="grado" id="grado" onfocus="" required style="width: 65px">
									  <option value="<?php echo $idgrado ?>"><?php echo $grado ?></option>
									</select>
                                </div>
							</div>
							<div class="col-md-3">
									<div class="form-group">
										<label for="" class="label-control">Sección</label>
										<select class="form-control" name="seccion" id="seccion" required style="width: 80px">
										  	<option value="<?php echo $idseccion ?>"><?php echo $seccion ?></option>
									  	</select>
								  	</div>
								</div>
							<div class="col-md-12">
								<div class="form-group"><label for="">e-mail</label><input type="text" class="form-control" name="email" value="<?php echo $email ?>"></input></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="">Dirección</label><input type="text" class="form-control" name="direccion" value="<?php echo $direccion ?>"></input></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div align="center">
								<a href="listaDocentes.php" class="btn btn-primary">Volver</a>
								<input type="submit" class="btn btn-primary" value="Guardar">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

</body>
</html>