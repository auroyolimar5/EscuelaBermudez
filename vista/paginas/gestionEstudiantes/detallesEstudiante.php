<?php 
include_once("../../../controladores/gestionEstudiante/con_detalleEstudiante.php");
?>
<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
	<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 align="left">Gestión de Usuarios/Detalles Estudiante</h4>
			<br>
		</div>
	</div>
</div>
<div class="container" id="form">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<div class="col-md-12">
						<label for="">Foto</label>
						<div align="center"><img src="<?php echo $datosEstudiante[13]?>" width="208" height="275"></div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="col-md-12"><h4>Datos del Estudiante</h4></div>
					<div class="col-md-6">
						<!-- <label for="">Columna 1</label> -->
						<div class="form-group"><label for="">Nombres</label><span type="text" class="form-control" ><?php echo $datosEstudiante[3].' '.$datosEstudiante[4]?></span></div>
						<div class="form-group"><label for="">Apellidos</label><span type="text" class="form-control"><?php echo $datosEstudiante[1].' '.$datosEstudiante[2]?></span></div>
						<div class="form-group"><label for="">Cédula</label><span type="text" class="form-control"><?php echo $datosEstudiante[6]?></span></div>
					</div>
					<div class="col-md-6">
						<!-- <label for="">Columna 2</label> -->
						<div class="col-md-6">
							<div class="form-group"><label for="">Grado</label><span class="form-control"><?php echo $datosEstudiante[7]?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Sección</label><span type="text" class="form-control"><?php echo $datosEstudiante[8]?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">¿Repitiente?</label><span type="text" class="form-control"><?php echo $datosEstudiante[10]?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Turno</label><span type="text" class="form-control"><?php echo $datosEstudiante[9]?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Nacionalidad</label><span type="text" class="form-control"><?php echo $datosEstudiante[5]?></span></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Fch. Nac.</label><span type="text" class="form-control"><?php echo $datosEstudiante[11]?></span></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group"><label for="">Dirección</label><span type="text" class="form-control"><?php echo $datosEstudiante[12]?></span></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div align="center">
							<a href="listaEstudiantes.php" class="btn btn-primary">Volver</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>