<?php 
include_once("../../../controladores/gestionEstudiante/con_fichaEstudiante.php");
extract($_REQUEST);
?>
<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
	<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 align="left">Consultas/Ficha del Estudiante</h4>
			<br>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<form action="../../../controladores/gestionEstudiante/con_fichaEstudiante.php" method="post">
			<div class="col-md-3">
				<div class="form-group">
					<input type="text" name="cedulaEst" class="form-control" placeholder="Cédula del estudiante">
					<input type="hidden" name="accion" value="1">
				</div>
			</div>
			<div class="col-md-3"><input type="submit" class="btn btn-primary" value="Buscar"></div>
		</form>
	</div>
</div>
<div class="container" id="form">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<div class="col-md-12">
						<label for="">Foto</label>
						<div align="center"><img src="<?php  if(isset($foto)){echo $foto;}?>" width="208" height="275"></div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="col-md-12"><h4>Datos del Estudiante</h4></div>
					<div class="col-md-6">
						<!-- <label for="">Columna 1</label> -->
						<div class="form-group"><label for="">Nombres</label><input type="text" class="form-control" name=""value="<?php if(isset($primerNombre)){ echo $primerNombre.' ';} if( isset($segundoNombre)){echo $segundoNombre;}?>"></div>
						<div class="form-group"><label for="">Apellidos</label><input type="text" class="form-control" value="<?php if(isset($primerApellido)){ echo $primerApellido.' ';} if( isset($segundoApellido)){echo $segundoApellido;}?>"></div>
						<div class="form-group"><label for="">Cédula</label><input type="text" class="form-control" value="<?php if(isset($cedulaestudiante)){ echo $cedulaestudiante;}?>"></div>
					</div>
					<div class="col-md-6">
						<!-- <label for="">Columna 2</label> -->
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Grado</label>
								<input type="text" class="form-control" value="<?php if(isset($grado)){ echo $grado;}?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Sección</label><input type="text" class="form-control" value="<?php if(isset($seccion)){ echo $seccion;}?>" ></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">¿Repitiente?</label><input type="text" class="form-control" value="<?php if(isset($repitiente)){ echo $repitiente;}?>"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Turno</label><input type="text" class="form-control" value="<?php if(isset($turno)){ echo $turno;}?>"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Nacionalidad</label><input type="text" class="form-control" value="<?php if(isset($nacionalidadEstudiante)){ echo $nacionalidadEstudiante;}?>"></div>
						</div>
						<div class="col-md-6">
							<div class="form-group"><label for="">Fch. Nac.</label><input type="text" class="form-control" value="<?php if(isset($fechaNac)){ echo $fechaNac;}?>"></div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group"><label for="">Dirección</label><input type="text" class="form-control" value="<?php if(isset($direccionEstudiante)){ echo $direccionEstudiante;}?>"></div>
					</div>
				</div>
				<div class="row">
					<dir class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12"><h4>Datos del Representante</h4></div>
								<div class="col-md-4">
									<div class="col-md-12">
										<div class="form-group"><label for="">Nombres</label><input type="text" class="form-control" value="<?php if(isset($primerNombreRep)){ echo $primerNombreRep.' '.$segundoNombreRep;} ?>"></div>
									</div>
									<div class="col-md-12">
										<div class="form-group"><label for="">Cédula</label><input type="text" name="cedula" class="form-control" value="<?php if(isset($cedulaRep)){ echo $cedulaRep;} ?>"></div>
									</div>
									<div class="col-md-12">
										<div class="form-group"><label for="">Lugar de Trabajo</label><input type="text" class="form-control" name="lTrabajo" value="<?php if(isset($lugarTrabajo)){ echo $lugarTrabajo;} ?>"></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12">
										<div class="form-group"><label for="">Apellidos</label><input type="text" name="apellidos" class="form-control" value="<?php if(isset($primerApellidoRep)){ echo $primerApellidoRep.' ';} if(isset($segundoApellidoRep)){echo $segundoApellidoRep;}?>"></div>
									</div>
                                                                        <div class="col-md-12">
										<div class="form-group"><label for="">Parentesco</label><input type="text" name="parentesco" class="form-control" value="<?php if(isset($parentesco)){ echo $parentesco;} ?>"></div>
									</div>
									<div class="col-md-12">
										<div class="form-group"><label for="">Email</label><input type="text" class="form-control" value="<?php if(isset($email)){ echo $email;} ?>"></intup></div>
									</div>
								</div>
								<div class="col-md-4">
                                                                        <div class="col-md-12">
										<div class="form-group"><label for="">Nacionalidad</label><input type="text" class="form-control" name="nacionalidad" value="<?php if(isset($nacionalidadRep)){ echo $nacionalidadRep;} ?>" ></div>
									</div>
									<div class="col-md-12">
										<div class="form-group"><label for="">Profesión u Oficio</label><input type="text" class="form-control" value="<?php if(isset($profesion)){echo $profesion;} ?>"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group"><label for="">Telefono Hab.</label><input type="text" class="form-control" value="<?php if(isset($numHabitacion)){ echo $numHabitacion;} ?>"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group"><label for="">Telefono Cel.</label><input type="text" class="form-control" value="<?php if(isset($numCelular)){ echo $numCelular;} ?>"></div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-12">
										<div class="form-group"><label for="">Dirección Habitación</label><input type="text" class="form-control" value="<?php if(isset($direccionRep)){ echo $direccionRep;} ?>"></div>
									</div>
								</div>
							</div>
						</div>
					</dir>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<?php 
	if (!empty($mensaje)){
	if($mensaje==1){echo"<script type='text/javascript'> alert('Cédula de idenidad no resgistrada en el sistema')</script>";}
	}
?>
