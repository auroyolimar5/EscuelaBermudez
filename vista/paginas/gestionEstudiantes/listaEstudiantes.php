<?php 
extract($_REQUEST);
if (isset($_GET['mensaje'])){
	$mensaje = $_GET['mensaje'];
	}else{
		$mensaje = "";
		}
include_once("../../../controladores/gestionEstudiante/con_listaEstudiantes.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link rel="stylesheet" type="text/css" href="../../Lib/bootstrap-3.2.0/css/bootstrap.css"/>
<style type="text/css">
</style>
<script language="JavaScript">
function confirmar ( mensaje ) 
	{
		return confirm ( mensaje ); 
	}
</script>

</head>

<body>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h4 align="left">Gestión de Usuarios/Lista de Estudiantes</h4>
      </br>
        <table class="table">
                <tr>
                  <th width="25%" ><div align="center"><strong>Nombres y Apellidos</strong></div></th>
                  <th width="25%" ><div align="center"><strong>Turno</strong></div></th>
                  <th width="20%" ><div align="center"><strong>Grado</strong></div></th>
                  <th width="20%" ><div align="center"><strong>Sección</strong></div></th>
                  <th colspan="3" width="10%"><div align="center"><strong>Acción</strong></div></th>
                </tr>
               <?php
             while ($fila=pg_fetch_array($datos))
            { 
             ?>
                  <tr>
                  <td id="accion"><div align="center"><?php echo $fila[3]." ".$fila[1]?></div></td>
                  <td><div align="center"><?php echo $fila[7]?></div></td>
                  <td><div align="center"><?php echo $fila[5]?></div></td>
                  <td><div align="center"><?php echo $fila[6]?></div></td>
                  <td><a href="../gestionEstudiantes/detallesEstudiante.php?id=<?php echo $fila['id']?>"> <span title="Detalles" style="font-size:20px;" class = "glyphicon glyphicon-eye-open"></span></a></td>
                  <td><a href="../gestionEstudiantes/editarEstudiante.php?id=<?php echo $fila['id']?>"> <span title="Editar" style="font-size:20px;" class = "glyphicon glyphicon-pencil"></span></a></td>
              <!-- insercion del href la imagen eliminar y asociaci�n a la cedula del usuario -->
                  <td><a href="../../../controladores/gestionEstudiante/con_eliminarEstudiante.php?id=<?php echo $fila['id']?>"><span style="font-size:20px;" class = "glyphicon glyphicon-remove" title=
                  "Eliminar" onclick="return confirmar ('¿Seguro desea eliminar los datos de este Estudiante?')"></span></a></td>
                </tr>
               <?php
            }
             ?>
        </table>
    </div>
  </div>
</div>       
<?php 
if ($mensaje==2){echo "<script language='javascript' type='text/javascript'>window.alert('Usuario editado exitosamente');</script>";}		
?>
</body>
</html>