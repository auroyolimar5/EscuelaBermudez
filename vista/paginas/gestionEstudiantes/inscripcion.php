<?php 
session_start();
extract($_REQUEST);
include_once('../../js/combosAjax.php');
if(isset($_GET['mensaje'])){
$mensaje = $_GET['mensaje'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ficha de Inscripción</title>
<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
<script language="javascript" src="../../lib/jquery-ui-1.11.2.custom/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../../lib/jquery-ui-1.11.2.custom/jquery-ui.css">
<script language="javascript" src="../../js/principal.js"> </script>
<link rel="stylesheet" type="text/css" href="../../css/css.css"/>´
<script>
//función del calendario
$(function() {
$( "#datepicker" ).datepicker({ maxDate: "0D" });
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: 'Anterior',
		nextText: 'Siguiente',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
		dayNamesShort: ['Dom','Lun','Mar','Mie;','Juv','Vie','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sab'],
		weekHeader: 'Sm',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		changeMonth: true,
		changeYear: true,
		yearRange: "1920:2014",
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});
</script>
</head>
<body>
<div class="container">
<div class="row">
    <div class="col-md-12" style="margin-top: -20px">
        <h4 align="left">Gestión de Estudiante/inscripción</h4>
        <br>
    </div>
</div>
</div>
    
<div class="container">
    <div class="row">
    <div class="col-md-12">
	
        <h4><span id="ficha">Paso 1 <span class="glyphicon glyphicon-chevron-right"></span></span><a href="#" onclick="mostrarFormEstudiante()"> Datos del Estudiante <span class="glyphicon glyphicon-file"></span></a></h4>
	<div id="form-estudiante">
	<div class="container" id="form">
	
<div class="row">
	

<form action="../../../controladores/gestionEstudiante/con_inscripcion.php" method="post" enctype="multipart/form-data">
    <div class="col-md-3 ">
    <label for="" class="label-control">Foto del Alumno</label>
    <div class="form-group">
    <input type="file" class="btn btn-primary" name="archivo" id="archivo" value="Examinar" onclick="" required/>
    </br>
    <input type="hidden" name="accion" value="1" />
    <input type="submit" value="Subir Foto" />
</div>
</div>

    
    <div class="col-md-2 col-md-offset-2" style="width: 100px">
    <div class="form-group">
            <label for="" class="label-control">Grado</label>
            <select class="form-control" name="idgrado" id="grado" onfocus="" required >
                    <option value="">--Seleccionar--</option>
            </select>
    </div>
</div>

<div class="col-md-2" style="width: 115px">
    <div class="form-group">
            <label for="" class="label-control">Sección</label>
            <select class="form-control" name="idseccion" id="seccion" required>
                    <option value="">--Seleccionar--</option>
            </select>
            	
    </div>
</div>
    
    <div class="col-md-2" style="width: 140px">
                <div class="form-grouphas-feedback">
                        <label class="label-control">Turno</label>
                        <select name="idturno" class="form-control" id="turno" required>
                                <option value="">--Seleccionar--</option>
                        </select>
                </div>
        </div>
	
            
    <div class="col-md-2">
    <div class="form-group">
    <label class="label-control">Imagen</label>
        <div id="imagen">
        <?php if(isset($ruta)){
            echo "<img src='".$ruta."' width='100' height='120' />";
            }
        ?>
        </div>
    </div>
</div>
</form>
</div>

        </div>
            <div class="container" id="form">
            
<form data-toggle="validator" action="../../../controladores/gestionEstudiante/con_inscripcion.php" method="post" name="form2">
    <div class="row">
        <div style="text-align: center">
        <div class="col-md-2 col-md-offset-3">
    <div >
            <label for="" class="label-control">Grado</label>
            <input type="text" name="grado" value="<?php echo $desGrado; ?>" disabled style="text-align: center"></input>
    </div>
</div>

<div class="col-md-2">
    <div >
            <label for="" class="label-control">Sección</label>
            <input type="text" name="seccion" value="<?php echo $desSeccion; ?>" disabled style="text-align: center"></input>
    </div>
</div>
    
    <div class="col-md-2">
    <div >
    <label class="label-control">Turno</label>
    <input type="text" name="turno" value="<?php echo $desTurno; ?>" disabled style="text-align: center"></input>
    </div>
    </div>
        </div>
    </div>
    
    
    
    <div class="row">
        <div class="col-md-3">
                <div class="form-group has-feedback">
                        <label class="label-control">Primer Apellido</label>
                        <input class="form-control text" name="primerApellidoEst" required><span class="erores"></span></input>

                </div>
        </div>
        <div class="col-md-3">
                <div class="form-group has-feedback">
                        <label class="label-control">Segundo Apellido</label>
                        <input class="form-control text" name="segundoApellidoEst"></input>
                </div>
        </div>
        <div class="col-md-3">
                <div class="form-group has-feedback">
                        <label class="label-control">Primer Nombre</label>
                        <input class="form-control text" name="primerNombreEst" required></input>
                </div>
        </div>
        <div class="col-md-3">
                <div class="form-group has-feedback">
                        <label class="label-control">Segundo Nombre</label>
                        <input class="form-control text" name="segundoNombreEst"></input>
                </div>
        </div>
</div>
    
<div class="row">
    
    <div class="col-md-1" >
                <div>
                        <label class="label-control">Nac.</label>
                        <select class="form-control" name="nacEst" required style="width: 60px">
                          <option value="Venezolano">V</option>
                          <option value="Extranjero">E</option>
                        </select>
                </div>
        </div>
    
<div class="row">
        <div class="col-md-2">
                <div class="form-group has-feedback">
                        <label class="label-control">C.I.</label>
                        <input class="form-control numeric" name="cedulaEst" required></input>
                </div>
        </div>
        <div class="col-md-2">
                <div>
                        <label class="label-control">Sexo</label>
                        <select class="form-control" name="sexoEst" required>
                          <option value=""selected="selected">seleccione</option>
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                        </select>
                </div>
        </div>
        
        <div class="col-md-3">
                <div class="form-group has-feedback">
<label class="laberl-control">Fecha de Nac</label>
<div class='input-group date' id="datetimepicker">
<input type='text' class="form-control" id="datepicker" name="fecNacEst" required/>
<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>
</div>
        </div>
        <div class="col-md-3">
                <div class="form-group has-feedback">
                        <label class="label-control">Lugar de Nacimiento</label>
                        <input class="form-control text" name="lugarNacEst" required></input>
                </div>
        </div>
</div>

</div>
    
<div class="row">
        <div class="col-md-12">
                <div class="form-group has-feedback">
                        <label class="label-control">Dirección de Habitación</label>
                        <input class="form-control" name="direccionEst" required></input>
                </div>
        </div>
        <div class="col-md-4">
                <div class="form-group has-feedback">
                <label class="label-control">Telf Habitación</label>
                        <div class="input-group">
                                <input class="form-control" name="telfhabEst" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
                        </div>
                </div>
        </div>
        <div class="col-md-4">
                <div class="form-group has-feedback">
                <label class="label-control">Telf Celular</label>
                        <div class="input-group">
                                <input class="form-control" name="telfCelEst" required></input>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>
                        </div>
                </div>
        </div>
</div>
<div class="row">
        <div class="col-md-4">
                <div class="form-group has-feedback">
                        <label class="label-control">¿Padece alguna enfermendad?</label>
                        <select class="form-control" name="enfermedadEst" required>
                          <option value="" selected="selected" required>Seleccione</option>
                          <option value="Si">SI</option>
                          <option value="No">NO</option>
                        </select>
                </div>
        </div>
        <div class="col-md-4">
                <div class="form-group has-feedback">
                        <label class="label-control">Especifíque</label>
                        <input class="form-control" name="enfespecifica"></input>
                </div>
        </div>
        <div class="col-md-4">
                <div class="form-group has-feedback">
                        <label class="label-control">¿Hermanos(as) en la institución?</label>
                        <select class="form-control" name="hermanos" required>
                          <option value="" selected="selected" required>Seleccione</option>
                          <option value="Si">SI</option>
                          <option value="No">NO</option>
                        </select>
                </div>
        </div>
</div>
<div class="row">
        <div class="col-md-2">
                <div class="form-grouphas-feedback">
                        <label for="" class="label-control">Grado</label>
                        <select name="gradoHerm" id="grado2" class="form-control" required>
                                <option value="">--Seleccionar--</option>
                        </select>
                </div>
        </div>
        <div class="col-md-2">
                <div class="form-grouphas-feedback">
                        <label for="" class="label-control">Sección</label>
                        <select name="seccionHerm" class="form-control" id="seccion2"required>
                                <option value="">--Seleccionar--</option>
                        </select>
                </div>
        </div>
<!--        <div class="col-md-2">
                <div class="form-grouphas-feedback">
                        <label class="label-control">Turno</label>
                        <select name="turnoHerm" class="form-control" id="turno" required>
                                <option value="">--Seleccionar--</option>
                        </select>
                </div>
        </div>-->
        <div class="col-md-2">
                <div class="form-grouphas-feedback">
                <label for="" class="label-control" >Año Escolar</label>
                <input type="text" name="anioEscolar" class="form-control" required/></div>
        </div>
        <div class="col-md-2">
                <div class="form-grouphas-feedback">
                        <label for="" class="label-control" >¿Repitiente?</label>
                        <select name="repitiente" class="form-control" required>
                                <option value="" selected="selected" >Seleccione</optio>
                                <option value="Si">SI</option>
                                <option value="No">NO</option>
                        </select>
                        <input type="hidden" name="Estudiante" value="1" />
                        <input type="hidden" name="rutaImagen" value="<?php if(isset($ruta)){echo $ruta;}?>" >
                        <input class="form-control numeric" type="hidden" name="idgrado2" value="<?php if(isset($idgrado)){echo $idgrado;}?>" >
                        <input class="form-control numeric" type="hidden" name="idseccion2" value="<?php if(isset($idseccion)){echo $idseccion;}?>" >
                        <input class="form-control numeric" type="hidden" name="idturno2" value="<?php if(isset($idturno)){echo $idturno;}?>" >
                </div>
        </div>
</div>
</div>
</div>



<div class="del Representante">
<h4><span id="ficha">Paso 2 <span class="glyphicon glyphicon-chevron-right"></span></span><a href="#" onclick="mostrarFormRepresentante()"> Datos del Representante <span class="glyphicon glyphicon-file"></span></a></h4>
<div id="form-representante">
<div class="container" id="form">
        <div class="row">
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Primer Apellido</label>
                                <input class="form-control text" name="primerApellidoRep" required></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Segundo Apellido</label>
                                <input class="form-control text" name="segundoApellidoRep"></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Primer Nombre</label>
                                <input class="form-control text" name="primerNombreRep" required></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Segundo Nombre</label>
                                <input class="form-control text" name="segundoNombreRep"></input>
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Nacionalidad</label>
                                <select class="form-control" name="nacRep">
                                        <option value="Venezolano(a)">Venezolano</option>
                                        <option value="Extranjero(a)">Extranjero</option>
                                </select>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Cédula de Identidad</label>
                                <input class="form-control numeric" name="cedulaRep"></input>
                        </div>								
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Parentesco</label>
                                <select class="form-control" name="parentescoRep">
                                        <option value="Padre">Padre</option>
                                        <option value="Madre">Madre</option>
                                        <option value="Abuelo(a)">Abuelo(a)</option>
                                        <option value="Tio(a)">Tio(a)</option>
                                        <option value="Hermano(a)">Hermano(a)</option>
                                        <option value="Primo(a)">Primo(a)</option>
                                        <option value="otro">otro</option>
                                </select>
                        </div>								                                
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Profesion u Oficio</label>
                                <select name="ocupacionRep" class="form-control" id="profesionOficio" required>
                                <option value="">--Seleccionar--</option>
                                </select>
                        </div>
                </div>
                <div class="col-md-12">
                        <div class="form-group has-feedback">
                                <label class="label-control">Lugar de Trabajo</label>
                                <input class="form-control" name="lugarTrabRep"></input>
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-md-12">
                        <div class="form-group has-feedback">
                                <label class="label-control">Dirección de Habitación</label>
                                <input class="form-control" name="direccionRep"></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Vive con el (la) estudiante</label>
                                <select class="form-control" name="viveConEstRep">
                                        <option value="Si">SI</option>
                                        <option value="No">NO</option>	
                                </select>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Telf Habitación</label>
                                <input class="form-control" name="telfhabRep"></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Telf Celular</label>
                                <input class="form-control" name="telfcelRep"></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">Telf Oficina</label>
                                <input class="form-control" name="telfoficRep"></input>
                        </div>
                </div>
                <div class="col-md-3">
                        <div class="form-group has-feedback">
                                <label class="label-control">E-mail</label>
                                <input class="form-control" name="emailRep"></input>
                                <input type="hidden" name="accion" value="2" />
                        </div>
                </div>
                <div class="col-md-12">
                        <div align="center">
                                <input type="submit" class="btn btn-primary" value="Guardar">
                                <input type="reset" class="btn btn-primary" value="Limpiar">
                        </div>
                </div>
        </div>
</div>
</div>
</div>


</form>

</div>
</div>
</div>
</body>
</html>
<?php 
if (!empty($mensaje)){
	if($mensaje==1){
	echo"
	<script type='text/javascript'>
	 	if(confirm('Se a Registrado la Inscripción Exitosamente \\n¿Desea imprimir el documento de Constancia?')){
			window.location='../getionReportes/constanciaInscripcion.php';
			}
	</script>";}
	if($mensaje==4){echo"<script type='text/javascript'>alert('El Regìsto no se ha realizado, El estudiante ya esta registrado en el sistema');</script>";}
}
 ?>