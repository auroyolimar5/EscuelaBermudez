<?php 
extract($_REQUEST);
		include_once("../../../controladores/gestionEstudiante/con_editarEstudiante.php");
		include_once('../../js/combosAjax.php');
?>
<!DOCTYPE html>
<html lang="en"><head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script language="javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
	<script language="javascript" src="../../lib/jquery-1.9.1.js"> </script>
	<script language="javascript" src="../../js/principal.js"> </script>
	<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 align="left">Gestión de Estudiantes/Detalles Estudiante</h4>
			<br>
		</div>
	</div>
</div>
<form action="../../../controladores/gestionEstudiante/con_guardaEstudiante.php" name="form1" method="post">
	<div class="container" id="form">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<div class="col-md-12">
							<label for="">Foto</label>
							<div align="center"><img src="<?php echo $datosEstudiante[13]?>" width="208" height="275"></div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="col-md-12"><h4>Datos del Estudiante</h4></div>
						<div class="col-md-6">
							<!-- <label for="">Columna 1</label> -->
							<div class="form-group"><label for="">Nombres</label>
                                                            <input type="text" name="nombre" class="form-control text" value="<?php echo $datosEstudiante[3].' '.$datosEstudiante[4]?>" disabled="on">
							</div>
                                                        <div class="form-group"><label for="">Apellidos</label><input type="text" class="form-control text" value="<?php echo $datosEstudiante[1].' '.$datosEstudiante[2]?>"name="apellido" disabled="on"/></div>
							<div class="form-group"><label for="">Cédula</label><input type="text" class="form-control numeric" value="<?php echo $datosEstudiante[6]?>"name="cedula" disabled="on"/></div>
						</div>
						<div class="col-md-6"> 
							<!-- <label for="">Columna 2</label> -->
							<div class="col-md-6">
								<div class="form-group"><label for="">Grado</label>
								<select name="grado" id="grado" class="form-control" required>
									<option value=""><?php echo $datosEstudiante[7]?></option>
								</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="">Sección</label>
								<select name="seccion" id="seccion" class="form-control" required>
									<option value=""><?php echo $datosEstudiante[8]?></option>
								</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="">¿Repitiente?</label>
                                                                    <select class="form-control" name="repitiente" required style="width: 70px">
                                                                        <option value=""><?php echo $datosEstudiante[10]?></option>
                                                                        <option value="Si">Si</option>
                                                                      <option value="No">No</option>
                                                                    </select>
                                                                    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="">Turno</label>
								<select name="turno" id="turno" class="form-control" required>
									<option value=""><?php echo $datosEstudiante[9]?></option>
								</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="">Nacionalidad</label>
                                                                    <select class="form-control" name="nacEst" required style="width: 60px" disabled="on">
                                                                        <option value=""><?php echo $datosEstudiante[5]?></option>
                                                                        <option value="Venezolano">Venezolano</option>
                                                                      <option value="Extranjero">Extranjero</option>
                                                                    </select>
                                                                    </div>
							</div>
<!--							<div class="col-md-6">
								<div class="form-group"><label for="">email</label><input type="text" class="form-control" value="<?php ?>" name="email"></div>
							</div>-->
							<div class="col-md-6">
								<div class="form-group"><label for="">Fch. Nac.</label><input type="text" class="form-control" value="<?php echo $datosEstudiante[11]?>" name="fechaNac"></div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"><label for="">Dirección</label><input type="text" class="form-control" value="<?php echo $datosEstudiante[12]?>" name="direccion"></div>
							<input type="hidden" name="id" value="<?php echo $id ?>">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div align="center">
								<a href="listaEstudiantes.php" class="btn btn-primary">Volver</a>					
								<input type="submit" class="btn btn-primary" value="Guardar" >
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

</body>
</html>