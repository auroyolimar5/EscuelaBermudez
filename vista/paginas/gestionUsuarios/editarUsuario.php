<?php 
extract($_REQUEST);
	$usuario = $_GET['id'];
if(isset($mensaje)){
	$mensaje = $_GET['mensaje'];
	}else{
		$mensaje = "";
		}
include_once("../../../controladores/gestionUsuarios/con_EditaUsuario.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 ">

<script type="text/javascript" src="../../lib/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"></script>
<script type="text/javascript" src="../../js/principal.js"></script>
<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
<title>Documento sin título</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12" >
			<h4>Datos del usuario</h4>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
					</br>
                       	<div class="col-md-6 col-md-offset-3">
	                       	<div id="form">
                            <form name="form1" method="post" action="../../../controladores/gestionUsuarios/con_EditaUsuario.php"> 
                            	
                                <div class="form-group has-feedback has-feedback-ri">
									<label>Usuario</label>
                                                                        <input type="text" name="identidad" class="form-control" Value="<?php echo $usuario;?>" disabled></input>
                                                                        <input type="hidden" name="usuario" class="form-control" Value="<?php echo $usuario;?>"
									<span class = "form-control-feedback glyphicon glyphicon-user"></span>
                                                                        
								</div>
                                
                                
                                
                                <div class="form-group">	
									<label>Nombre</label>
								  <input type="text" name="nombre" class="form-control text" Value="<?php echo $nombre;?>" required></input>
								</div>
								<div class="form-group">
									<label>Apellido</label>
								  <input type="text" name="apellido" class="form-control text"  Value="<?php echo $apellido;?>" required></input>
								</div>
                                <div class="form-group">
									<label>N° Cédula</label>
								  <input type="text" name="cedula" class="form-control numeric" Value="<?php echo $cedula;?>" required></input>
								</div>
								<div class="form-group has-feedback has-feedback-ri">
									<label>E-mail</label>
								  <input type="email" name="email" class="form-control"  Value="<?php echo $email;?>" required>
									<span class = "form-control-feedback glyphicon glyphicon-envelope"></span> 
								</div>
								
                                <div class="form-group has-feedback  has-feedback-ri">
									<label>Tipo</label>
									<select name="categoria" class="form-control"required>
                                      <option value="<?php echo $categoria;?>" selected="selected"><?php echo $categoria;?></option>
									  <option value="Super-Admin">Super_Admin</option>
									  <option value="Admin">Admin</option>
									  <option value="Profesor">Profesor</option>
									  <option value="Director">Director</option>                                    
                                      <option value="Secretaria">Secretaria</option>
                                      <option value="Sub-director">Sub-Director</option>
                                      
									</select>
								</div>
                                <input type="hidden" name="operacion" value="1"/>
                                 <input type="hidden" name="id" value="<?php echo $var2?>"/>
                                <div align="center">
                                	<a href="listaUsuarios.php" class="btn btn-primary">Volver</a>
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                              </div>
                            </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($mensaje !=""){echo"<script type='text/javascript'>alert('Registro de Usuario Exitoso');</script>";}?>
</body>
</html>