<?php 
extract($_REQUEST);
if(isset($_GET['id'])){
	$usuario = $_GET['id'];
	}else{
		$var2 = "";
		}
include_once("../../../controladores/gestionUsuarios/con_DetalleUsuario.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 ">

<script type="text/javascript" src="../../lib/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"></script>
<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
<title>Documento sin título</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12" >
			<h4>Datos del usuario</h4>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
					</br>
                       	<div class="col-md-6 col-md-offset-3">
	                       	<div id="form">
								<div class="form-group has-feedback has-feedback-ri">
									<label>Usuario</label>
                                                                        <input type="text" name="usuario" class="form-control" Value="<?php echo $usuario;?>" disabled></input>
									<span class = "form-control-feedback glyphicon glyphicon-user"></span> 
								</div>
                                                                <div class="form-group">	
									<label>Nombre</label>
                                                                        <input type="text" name="PrimerNombre" class="form-control" Value="<?php echo $nombre;?>" disabled></input>
								</div>
								<div class="form-group">
									<label>Apellido</label>
								  <input type="text" name="PrimerApellido" class="form-control"  Value="<?php echo $apellido;?>" disabled></input>
								</div>
                                <div class="form-group">
									<label>N° Cédula</label>
								  <input type="text" name="cedula" class="form-control" Value="<?php echo $cedula;?>" disabled></input>
								</div>
								<div class="form-group has-feedback has-feedback-ri">
									<label>E-mail</label>
								  <input type="email" name="email" class="form-control"  Value="<?php echo $email;?>" disabled>
									<span class = "form-control-feedback glyphicon glyphicon-envelope"></span> 
								</div>
								
							    <div class="form-group has-feedback  has-feedback-ri">
									<label>Tipo</label>
									<input type="text" name="tipo" class="form-control" Value="<?php echo $categoria;?>" disabled>
								</div>
                                <div align="center">
                                    <a href="listaUsuarios.php" class="btn btn-primary">Volver</a>
                              </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>