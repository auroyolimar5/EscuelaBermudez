<?php
extract($_REQUEST);
if (isset($_GET['mensaje'])){
$mensaje = $_GET['mensaje'];
}else{$mensaje="";}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1 ">

<script type="text/javascript" src="../../lib/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../lib/bootstrap-3.2.0/js/bootstrap.js"></script>
<script type="text/javascript" src="../../js/principal.js"></script>
<link rel="stylesheet" type="text/css" href="../../lib/bootstrap-3.2.0/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../../css/css.css"/>
<title>Documento sin título</title>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-12" >
			<h4>Datos del usuario</h4>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
					</br>
                       	<div class="col-md-6 col-md-offset-3">
	                       	<div id="form">
                            	<form  id="form1" name="form1" method="post" action="../../../controladores/gestionUsuarios/conCrearUsuario.php">
								<div class="form-group">	
									<label>Nombre</label>
									<input type="text" name="nombre" class="form-control text" placeholder="Nombre" required></input>
								</div>
								<div class="form-group">
									<label>Apellido</label>
									<input type="text" name="apellido" class="form-control text" placeholder="Apellido" required></input>
								</div>
                                <div class="form-group">
									<label>N° Cédula</label>
									<input type="text" name="cedula" class="form-control numeric" placeholder="Número de Cédula" required></input>
								</div>
								<div class="form-group has-feedback has-feedback-ri">
									<label>E-mail</label>
									<input type="email" name="email" class="form-control" placeholder="tu_email@dominio.com" required>
									<span class = "form-control-feedback glyphicon glyphicon-envelope"></span> 
								</div>
								<div class="form-group has-feedback has-feedback-ri">
									<label>Usuario</label>
									<input type="text" name="usuario" class="form-control" required></input>
									<span class = "form-control-feedback glyphicon glyphicon-user"></span> 
								</div>
								<div class="form-group has-feedback  has-feedback-ri">
									<label>Password</label>
									<input type="password" name="clave" class="form-control" required></input>
									<span class = "form-control-feedback glyphicon glyphicon-lock"></span> 
								</div>
                                <div class="form-group has-feedback  has-feedback-ri">
									<label>Tipo</label>
                                    <option value="<?php echo $tipo;?>" selected="selected"></option>
									<select name="categoria" class="form-control"required>
									  <option value="Admin">Admin</option>
									  <option value="Profesor">Profesor</option>
									  <option value="Secretaria">Secretaria</option>
									  <option value="Director">Director</option>
									  <option value="Representante">Usuario</option>
									</select>
								</div>
                                <div align="center">
                                
                                    <input name="Restablecer" type="reset" class="btn btn-primary" value="Limpiar">
                                    <input type="submit" class="btn btn-primary" value="Guardar">
								</div>
              					</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
if($mensaje==1){echo"<script type='text/javascript'>alert('El usuario fue registrado exitosamente');</script>";}
if($mensaje==2){echo"<script type='text/javascript'>alert('El Usuario ya existe intente con otro nombre de usuario');</script>";}
?>
</body>
</html>