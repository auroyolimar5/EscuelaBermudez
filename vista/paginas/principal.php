<?php 
session_start();
include_once('../../controladores/usuarios/validaSesion.php');
//echo'aqui'.$_SESSION['tipo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Principal</title>
<script src="../lib/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../lib/bootstrap-3.2.0/js/bootstrap.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../lib/bootstrap-3.2.0/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../css/css.css"/>
</head>

<body>
<!-- incio contenedor del encabezado -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- incio encabezado -->
			<div class="header1">
            	<img src="../img/Banner MPPPE.jpg" align="left"  class="img-responsive" alt="Responsive image" />
            </div>
			<!-- fin encabezado -->
            <h3></h3>
		</div>
	</div>
</div>
<!-- fin contenedor del encabezado -->
<!-- separación -->
<hr id="separacion"/>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- incio cuerpo -->
			<!-- incio menu principal -->
			<div id="menuPrincipal"> <?php include_once("menuPrincipal.php");?></div>
			<!-- fin menu principal -->
			 <div id="contenido">
                             <iframe src="Portada.php" name="contenido" scrolling="No" frameborder="0" id="contenido"></iframe>
                         </div>
			<hr id="separacion"/>
			<div class="piePagina">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                            <p align="center">Av. Principal de la vega, Calle Cementerio, TELEFONOS:(0212) 825-55-67 Email: uedbermudez@gmail.com</p>
                                    </div>
                                </div>
                            </div>
			</div>
			<!-- fin cuerpo -->
		</div>
	</div>
</div>
</body>
</html>