
--
-- Estrutura da tabela 'enfermedad'
--

DROP TABLE enfermedad CASCADE;
CREATE TABLE enfermedad (
enfermedad varchar(45) DEFAULT NULL::character varying,
persona_idpersona int4 NOT NULL,
idenfermedad int4 NOT NULL DEFAULT nextval('persona_idpersona_seq'::regclass)
);

--
-- Creating data for 'enfermedad'
--



--
-- Creating index for 'enfermedad'
--

ALTER TABLE ONLY  enfermedad  ADD CONSTRAINT  enfermedad_pkey  PRIMARY KEY  (idenfermedad);

--
-- Estrutura da tabela 'estudiante'
--

DROP TABLE estudiante CASCADE;
CREATE TABLE estudiante (
seccion_idseccion int4 NOT NULL,
status varchar(45) NOT NULL,
annoescolar varchar(10) NOT NULL,
grado_idgrado int4 NOT NULL,
repitiente varchar(2) NOT NULL,
per_idpersona int4 NOT NULL,
foto varchar(45) DEFAULT NULL::character varying,
hemanos_institucion varchar(2) NOT NULL,
tur_idturno int4 NOT NULL,
idestudiante int4 NOT NULL
);

--
-- Creating data for 'estudiante'
--



--
-- Creating index for 'estudiante'
--

ALTER TABLE ONLY  estudiante  ADD CONSTRAINT  estudiante_pkey  PRIMARY KEY  (idestudiante);

--
-- Estrutura da tabela 'estudiante_padres'
--

DROP TABLE estudiante_padres CASCADE;
CREATE TABLE estudiante_padres (
pad_idpadre int4 NOT NULL,
pad_idmadre int4 NOT NULL,
est_idestudiante int4 NOT NULL
);

--
-- Creating data for 'estudiante_padres'
--


--
-- Estrutura da tabela 'grado'
--

DROP TABLE grado CASCADE;
CREATE TABLE grado (
grado varchar(45) DEFAULT NULL::character varying,
idgrado int4 NOT NULL
);

--
-- Creating data for 'grado'
--

INSERT INTO grado VALUES ('2','2�');
INSERT INTO grado VALUES ('1','1�');
INSERT INTO grado VALUES ('3','3�');
INSERT INTO grado VALUES ('4','4�');
INSERT INTO grado VALUES ('5','5�');
INSERT INTO grado VALUES ('6','6�');
INSERT INTO grado VALUES ('7','7�');
INSERT INTO grado VALUES ('8','8�');
INSERT INTO grado VALUES ('9','9�');


--
-- Creating index for 'grado'
--

ALTER TABLE ONLY  grado  ADD CONSTRAINT  grado_pkey  PRIMARY KEY  (idgrado);

--
-- Estrutura da tabela 'padres'
--

DROP TABLE padres CASCADE;
CREATE TABLE padres (
prof_idprofecionofici int4 NOT NULL,
per_idpersona int4 NOT NULL,
idpadre int4 NOT NULL,
vive_con_estudiante varchar(2) NOT NULL
);

--
-- Creating data for 'padres'
--



--
-- Creating index for 'padres'
--

ALTER TABLE ONLY  padres  ADD CONSTRAINT  padres_pkey  PRIMARY KEY  (idpadre);

--
-- Estrutura da tabela 'persona'
--

DROP TABLE persona CASCADE;
CREATE TABLE persona (
lugartrabajo varchar(45) DEFAULT NULL::character varying,
primernombre varchar(25) NOT NULL,
primerapellido varchar(25) NOT NULL,
segundoapellido varchar(25) DEFAULT NULL::character varying,
segundonombre varchar(25) DEFAULT NULL::character varying,
direccionhab varchar(125) DEFAULT NULL::character varying,
email varchar(45) NOT NULL,
idpersona int4 NOT NULL DEFAULT nextval('persona_idpersona_seq'::regclass),
cedulaidentidad int4 NOT NULL,
nacionalidad bpchar NOT NULL,
fechanac date NOT NULL
);

--
-- Creating data for 'persona'
--

INSERT INTO persona VALUES ('Administrator','Administrator',NULL,NULL,' ','16085382','1990-01-01','milcky8887@hotmail.com','La Vega','La Vega','1');
INSERT INTO persona VALUES ('aurora','hernandez',NULL,NULL,'V','324234','2015-10-20',NULL,'sdaw wdqw',NULL,'4');
INSERT INTO persona VALUES ('adrian','guedez',NULL,NULL,'V','3424','2015-10-20',NULL,'weqeq',NULL,'5');
INSERT INTO persona VALUES ('milagros','bonilla',NULL,NULL,'V','17671816','2015-10-19','milcky88887@hotmail.com',NULL,NULL,'2');
INSERT INTO persona VALUES ('eleiny','brice�o',NULL,NULL,'V','13223232','2015-10-25','jona@gmail.com',NULL,NULL,'6');
INSERT INTO persona VALUES ('jessica','orozco','nathaly','castrillon','V','15491753','1990-09-16','dg.torreso@gmail.com','caracas','U.E.D. Bermudez','3');
INSERT INTO persona VALUES ('jhonatan ','rico',NULL,NULL,'V','12213123','1985-11-15','jonatha@gmail.com',NULL,NULL,'7');
INSERT INTO persona VALUES ('Roselmy','Pena','DelValle','Dayar','V','10515991','1969-09-09','roselmy69@hotmail.com','La Vega','U.E.D. Bermudez','8');
INSERT INTO persona VALUES ('maria','perez',NULL,NULL,'V','2222222','2015-10-16','maria@gmail.com','las acacias','U.E.N. Rep�blica del Ecuador','27');


--
-- Creating index for 'persona'
--

ALTER TABLE ONLY  persona  ADD CONSTRAINT  idpersona  PRIMARY KEY  (idpersona);

--
-- Estrutura da tabela 'profecionofici'
--

DROP TABLE profecionofici CASCADE;
CREATE TABLE profecionofici (
profeionoficio varchar(45) DEFAULT NULL::character varying,
idprofecionofici int4 NOT NULL
);

--
-- Creating data for 'profecionofici'
--

INSERT INTO profecionofici VALUES ('1','Obrero(a)');
INSERT INTO profecionofici VALUES ('2','Maestro(a)');
INSERT INTO profecionofici VALUES ('3','Medico');
INSERT INTO profecionofici VALUES ('4','Enfermero(a)');
INSERT INTO profecionofici VALUES ('5','Pintor(a)');
INSERT INTO profecionofici VALUES ('6','Polic�a');
INSERT INTO profecionofici VALUES ('7','Profesor(a)');
INSERT INTO profecionofici VALUES ('8','Alba�il');
INSERT INTO profecionofici VALUES ('9','Carpintero(a)');
INSERT INTO profecionofici VALUES ('10','Comerciante');
INSERT INTO profecionofici VALUES ('11','M�sico');
INSERT INTO profecionofici VALUES ('12','Vigilante');
INSERT INTO profecionofici VALUES ('13','Contador(a)');
INSERT INTO profecionofici VALUES ('14','Administrador(a)');
INSERT INTO profecionofici VALUES ('15','Analista(a)');
INSERT INTO profecionofici VALUES ('16','Tec.Informatico');
INSERT INTO profecionofici VALUES ('17','Ingeniero Civil(a)');
INSERT INTO profecionofici VALUES ('18','Ama de Casa');
INSERT INTO profecionofici VALUES ('19','Otra');


--
-- Creating index for 'profecionofici'
--

ALTER TABLE ONLY  profecionofici  ADD CONSTRAINT  profecionofici_pkey  PRIMARY KEY  (idprofecionofici);

--
-- Estrutura da tabela 'profesor'
--

DROP TABLE profesor CASCADE;
CREATE TABLE profesor (
idprofesor int4 NOT NULL,
status varchar(45) NOT NULL,
asignatura varchar(45) DEFAULT NULL::character varying,
per_idpersona int4 NOT NULL,
foto varchar(45) DEFAULT NULL::character varying
);

--
-- Creating data for 'profesor'
--

INSERT INTO profesor VALUES ('8','Docente de Aula','1',NULL,'8');


--
-- Creating index for 'profesor'
--

ALTER TABLE ONLY  profesor  ADD CONSTRAINT  profesor_pkey  PRIMARY KEY  (idprofesor);

--
-- Estrutura da tabela 'profesor_grado'
--

DROP TABLE profesor_grado CASCADE;
CREATE TABLE profesor_grado (
pro_idprofesor int4 NOT NULL,
gra_idgrado int4 NOT NULL
);

--
-- Creating data for 'profesor_grado'
--


--
-- Estrutura da tabela 'profesor_seccion'
--

DROP TABLE profesor_seccion CASCADE;
CREATE TABLE profesor_seccion (
sec_idseccion int4 NOT NULL,
prof_idprofesor int4 NOT NULL
);

--
-- Creating data for 'profesor_seccion'
--


--
-- Estrutura da tabela 'profesor_turno'
--

DROP TABLE profesor_turno CASCADE;
CREATE TABLE profesor_turno (
pro_idprofesor int4 NOT NULL,
tur_idturno int4 NOT NULL
);

--
-- Creating data for 'profesor_turno'
--


--
-- Estrutura da tabela 'representante'
--

DROP TABLE representante CASCADE;
CREATE TABLE representante (
prof_idprofecionofici int4 NOT NULL,
estudiante_grado_idgrado int4 NOT NULL,
estudiante_seccion_idseccion int4 NOT NULL,
estudiante_idestudiante int4 NOT NULL,
parentesco varchar(45) NOT NULL,
per_idpersona int4 NOT NULL,
idrepresentante int4 NOT NULL,
vive_con_estudiante varchar(2) NOT NULL
);

--
-- Creating data for 'representante'
--



--
-- Creating index for 'representante'
--

ALTER TABLE ONLY  representante  ADD CONSTRAINT  representante_pkey  PRIMARY KEY  (idrepresentante);

--
-- Estrutura da tabela 'seccion'
--

DROP TABLE seccion CASCADE;
CREATE TABLE seccion (
idseccion int4 NOT NULL,
seccion varchar(45) DEFAULT NULL::character varying,
status varchar(45) NOT NULL
);

--
-- Creating data for 'seccion'
--

INSERT INTO seccion VALUES ('1','A - 1','Manana');
INSERT INTO seccion VALUES ('2','A - 2','Manana');
INSERT INTO seccion VALUES ('3','A - 3','Manana');
INSERT INTO seccion VALUES ('4','B - 1','Tarde');
INSERT INTO seccion VALUES ('5','B - 2','Tarde');
INSERT INTO seccion VALUES ('6','B - 3','Tarde');


--
-- Creating index for 'seccion'
--

ALTER TABLE ONLY  seccion  ADD CONSTRAINT  seccion_pkey  PRIMARY KEY  (idseccion);

--
-- Estrutura da tabela 'telefono'
--

DROP TABLE telefono CASCADE;
CREATE TABLE telefono (
celular varchar(45) DEFAULT NULL::character varying,
oficina varchar(45) DEFAULT NULL::character varying,
persona_idpersona int4 NOT NULL DEFAULT nextval('telefono_persona_idpersona_seq'::regclass),
habitacion varchar(45) DEFAULT NULL::character varying,
idtelefono int4 NOT NULL DEFAULT nextval('telefono_idtelefono_seq'::regclass)
);

--
-- Creating data for 'telefono'
--



--
-- Creating index for 'telefono'
--

ALTER TABLE ONLY  telefono  ADD CONSTRAINT  telefono_pkey  PRIMARY KEY  (idtelefono);

--
-- Estrutura da tabela 'turno'
--

DROP TABLE turno CASCADE;
CREATE TABLE turno (
turno varchar(45) DEFAULT NULL::character varying,
idturno int4 NOT NULL DEFAULT nextval('turno_idturno_seq'::regclass)
);

--
-- Creating data for 'turno'
--

INSERT INTO turno VALUES ('Mana�a','3');
INSERT INTO turno VALUES ('Tarde','4');


--
-- Creating index for 'turno'
--

ALTER TABLE ONLY  turno  ADD CONSTRAINT  turno_pkey  PRIMARY KEY  (idturno);

--
-- Estrutura da tabela 'usuario'
--

DROP TABLE usuario CASCADE;
CREATE TABLE usuario (
status int4,
persona_idpersona int4 NOT NULL DEFAULT nextval('usuario_persona_idpersona_seq'::regclass),
clave varchar(45) NOT NULL,
tipo varchar(25) NOT NULL,
idusuario int4 NOT NULL DEFAULT nextval('usuario_idusuario_seq'::regclass),
usuario varchar(45) NOT NULL
);

--
-- Creating data for 'usuario'
--

INSERT INTO usuario VALUES ('admin','123456','Super-Admin','1','1','1');
INSERT INTO usuario VALUES ('jona','123','Usuario','1','12','12');
INSERT INTO usuario VALUES ('aurora','123','Secretaria','1','4','4');
INSERT INTO usuario VALUES ('adrian','123','Usuario','1','5','5');
INSERT INTO usuario VALUES ('eleiny','123','Profesor','1','6','6');
INSERT INTO usuario VALUES ('jose','123','Usuario','1','11','8');
INSERT INTO usuario VALUES ('mila','123','Sub-Director','1','2','2');
INSERT INTO usuario VALUES ('jess','123','Director','1','3','3');
INSERT INTO usuario VALUES ('maria','123','Usuario','1','13','24');
INSERT INTO usuario VALUES ('carina','123','Usuario','0','14','25');


--
-- Creating index for 'usuario'
--

ALTER TABLE ONLY  usuario  ADD CONSTRAINT  idusuario  PRIMARY KEY  (idusuario);


--
-- Creating relacionships for 'enfermedad'
--

ALTER TABLE ONLY enfermedad ADD CONSTRAINT enfermedad_persona_idpersona_fkey FOREIGN KEY (persona_idpersona) REFERENCES persona(idpersona);

--
-- Creating relacionships for 'estudiante'
--

ALTER TABLE ONLY estudiante ADD CONSTRAINT estudiante_per_idpersona_fkey FOREIGN KEY (per_idpersona) REFERENCES persona(idpersona);

--
-- Creating relacionships for 'estudiante'
--

ALTER TABLE ONLY estudiante ADD CONSTRAINT estudiante_tur_idturno_fkey FOREIGN KEY (tur_idturno) REFERENCES turno(idturno);

--
-- Creating relacionships for 'estudiante'
--

ALTER TABLE ONLY estudiante ADD CONSTRAINT estudiante_seccion_idseccion_fkey FOREIGN KEY (seccion_idseccion) REFERENCES seccion(idseccion);

--
-- Creating relacionships for 'estudiante'
--

ALTER TABLE ONLY estudiante ADD CONSTRAINT estudiante_grado_idgrado_fkey FOREIGN KEY (grado_idgrado) REFERENCES grado(idgrado);

--
-- Creating relacionships for 'estudiante_padres'
--

ALTER TABLE ONLY estudiante_padres ADD CONSTRAINT estudiante_padres_est_idestudiante_fkey FOREIGN KEY (est_idestudiante) REFERENCES estudiante(idestudiante);

--
-- Creating relacionships for 'padres'
--

ALTER TABLE ONLY padres ADD CONSTRAINT padres_per_idpersona_fkey FOREIGN KEY (per_idpersona) REFERENCES persona(idpersona);

--
-- Creating relacionships for 'padres'
--

ALTER TABLE ONLY padres ADD CONSTRAINT padres_prof_idprofecionofici_fkey FOREIGN KEY (prof_idprofecionofici) REFERENCES profecionofici(idprofecionofici);

--
-- Creating relacionships for 'profesor'
--

ALTER TABLE ONLY profesor ADD CONSTRAINT profesor_per_idpersona_fkey FOREIGN KEY (per_idpersona) REFERENCES persona(idpersona);

--
-- Creating relacionships for 'profesor_grado'
--

ALTER TABLE ONLY profesor_grado ADD CONSTRAINT profesor_grado_pro_idprofesor_fkey FOREIGN KEY (pro_idprofesor) REFERENCES profesor(idprofesor);

--
-- Creating relacionships for 'profesor_grado'
--

ALTER TABLE ONLY profesor_grado ADD CONSTRAINT profesor_grado_gra_idgrado_fkey FOREIGN KEY (gra_idgrado) REFERENCES grado(idgrado);

--
-- Creating relacionships for 'profesor_seccion'
--

ALTER TABLE ONLY profesor_seccion ADD CONSTRAINT profesor_seccion_prof_idprofesor_fkey FOREIGN KEY (prof_idprofesor) REFERENCES profesor(idprofesor);

--
-- Creating relacionships for 'profesor_seccion'
--

ALTER TABLE ONLY profesor_seccion ADD CONSTRAINT profesor_seccion_sec_idseccion_fkey FOREIGN KEY (sec_idseccion) REFERENCES seccion(idseccion);

--
-- Creating relacionships for 'profesor_turno'
--

ALTER TABLE ONLY profesor_turno ADD CONSTRAINT profesor_turno_pro_idprofesor_fkey FOREIGN KEY (pro_idprofesor) REFERENCES profesor(idprofesor);

--
-- Creating relacionships for 'profesor_turno'
--

ALTER TABLE ONLY profesor_turno ADD CONSTRAINT profesor_turno_tur_idturno_fkey FOREIGN KEY (tur_idturno) REFERENCES turno(idturno);

--
-- Creating relacionships for 'representante'
--

ALTER TABLE ONLY representante ADD CONSTRAINT representante_estudiante_grado_idgrado_fkey FOREIGN KEY (estudiante_grado_idgrado) REFERENCES grado(idgrado);

--
-- Creating relacionships for 'representante'
--

ALTER TABLE ONLY representante ADD CONSTRAINT representante_per_idpersona_fkey FOREIGN KEY (per_idpersona) REFERENCES persona(idpersona);

--
-- Creating relacionships for 'representante'
--

ALTER TABLE ONLY representante ADD CONSTRAINT representante_estudiante_idestudiante_fkey FOREIGN KEY (estudiante_idestudiante) REFERENCES estudiante(idestudiante);

--
-- Creating relacionships for 'representante'
--

ALTER TABLE ONLY representante ADD CONSTRAINT representante_prof_idprofecionofici_fkey FOREIGN KEY (prof_idprofecionofici) REFERENCES profecionofici(idprofecionofici);

--
-- Creating relacionships for 'representante'
--

ALTER TABLE ONLY representante ADD CONSTRAINT representante_estudiante_seccion_idseccion_fkey FOREIGN KEY (estudiante_seccion_idseccion) REFERENCES seccion(idseccion);

--
-- Creating relacionships for 'telefono'
--

ALTER TABLE ONLY telefono ADD CONSTRAINT telefono_persona_idpersona_fkey FOREIGN KEY (persona_idpersona) REFERENCES persona(idpersona);