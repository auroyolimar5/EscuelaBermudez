<?php
session_start();
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('../examples/tcpdf_include.php');
include_once ('../../../../controladores/gestionReportes/con_constanciaInscripcion2.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH/*, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING*/);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
$date = 'jesus';

//traduccion del la funcion date de php
$dia=date("l");

if ($dia=="Monday") $dia="Lunes";
if ($dia=="Tuesday") $dia="Martes";
if ($dia=="Wednesday") $dia="Miércoles";
if ($dia=="Thursday") $dia="Jueves";
if ($dia=="Friday") $dia="Viernes";
if ($dia=="Saturday") $dia="Sabado";
if ($dia=="Sunday") $dia="Domingo";

$mes=date("F");

if ($mes=="January") $mes="Enero";
if ($mes=="February") $mes="Febrero";
if ($mes=="March") $mes="Marzo";
if ($mes=="April") $mes="Abril";
if ($mes=="May") $mes="Mayo";
if ($mes=="June") $mes="Junio";
if ($mes=="July") $mes="Julio";
if ($mes=="August") $mes="Agosto";
if ($mes=="September") $mes="Setiembre";
if ($mes=="October") $mes="Octubre";
if ($mes=="November") $mes="Noviembre";
if ($mes=="December") $mes="Diciembre";

$ano=date("Y");

// create some HTML content
$html = '
<table>
	<tr>
		<td align="left"><img src="../../../img/Insignia.png" width="69" height="85" /></td>
		<td></td>
		<td align="right"><dt>La Vega, Caracas</dt><dt>Fecha: '.date("d/m/y").'</dt><dt> </dt></td>
	</tr>
</table>

<dt></dt>
<dt></dt>
<dt></dt>
<div align="center">
	<h2>Constancia de Inscripción</h2>
</div>
<div>
	<dt>A quien corresponda:</dt>
	<dt></dt>
	<dt></dt>
	<dt>	La Dirección de la U.E.D: "Bermúdez", hace constar que el (la) estudiante: <strong>'.$nombre.' '.$apellido.'</strong>,
	 quedó inscrito (a) en esta Intitucion en el <strong>'.$grado.'°</strong> grado, seccion <strong>"'.$seccion.'"</strong>, del turno de la <strong>"'.$turno.'"</strong> , en el año escolar <strong>'.$annoEscolar.'</strong>.</dt>
	<dt></dt>
	<dt></dt>	
	<dt>	Constancia que se expide a petición de la parte interesad en Caracas a los '.date("d").' días del mes de '.$mes.' del año '.$ano.'.</dt>
	
</div>

<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<div align="center">
	<dt>________________________________</dt>
	<dt> Prof. María Torres Briceño.</dt>
	<dt><strong> Director(E)</strong></dt>
</div>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<dt></dt>
<div>
	<span style="font-size: small;">Parroquia La Vega, Calle Cementerio TELEFONOS:(0212) 825-67-87 Email: uedbermudez@gmail.com</span>
</div>	
';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Constancia_de_Inscripción.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
//destruye la variable de session
unset( $_SESSION['idEstudiante'] );