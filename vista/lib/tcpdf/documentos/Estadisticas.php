<?php session_start();
include_once('../../../../modelo/clsDatos.php');

$sql= "select count(*) from estudiante where sexo_est='M'";
$contador_m= new Datos();
$datos= $contador_m->filtro($sql);
$arreglo_m = $contador_m->proximo($datos);
$contador_m->cerrarFiltro($datos);
$contador_m->cerrarConexion();
//echo $arreglo_m[0];
$total_m= $arreglo_m[0];

$sql= "select count(*) from estudiante where sexo_est='F'";
$contador_f= new Datos();
$datos= $contador_f->filtro($sql);
$arreglo_f = $contador_f->proximo($datos);
$contador_f->cerrarFiltro($datos);
$contador_f->cerrarConexion();
//echo $arreglo_f[0];
$total_f= $arreglo_f[0];



//class estadistica{
//
//	function getAllInfo(){
//		$sql = "SELECT * FROM vista_estudiante";
//		return $this->cerrarConexion();
//	}
//}
?>

 
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Highcharts Example</title>

                <script type="text/javascript" src="../../Highcharts-4.1.9/js/jquery.min.js"></script>
		<script src="../../Highcharts-4.1.9/js/highcharts.js"></script>
		<script src="../../Highcharts-4.1.9/js/highcharts-3d.js"></script>
		<script src="../../Highcharts-4.1.9/js/modules/exporting.js"></script>
		<style type="text/css">

${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Estadisticas de inscripciones por genero'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Inscritos',
            data: [
                ['Masculino',   <?php echo $total_m; ?>],
                ['Femenino',       <?php echo $total_f; ?>]
//                {
//                    name: 'Chrome',
//                    y: 12.8,
//                    sliced: true,
//                    selected: true
//                },
//                ['Safari',    8.5],
//                ['Opera',     6.2],
//                ['Others',   0.7]
            ]
        }]
    });
});
		</script>
	</head>
	<body>

<!--                <script src="../../Highcharts-4.1.9/js/highcharts.js"></script>
		<script src="../../Highcharts-4.1.9/js/highcharts-3d.js"></script>
		<script src="../../Highcharts-4.1.9/js/modules/exporting.js"></script>-->

<div id="container" style="height: 400px"></div>
	</body>
</html>