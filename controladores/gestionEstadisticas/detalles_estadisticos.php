&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"&gt;
&lt;html&gt;
	&lt;head&gt;
	 &lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt;
	   &lt;title&gt;Estadisticas con Jquery | Jquery Easy&lt;/title&gt;
		&lt;script type="text/javascript" src="js/jquery-1.4.2.min.js"&gt;&lt;/script&gt;
		&lt;script type="text/javascript" src="js/highcharts.js"&gt;&lt;/script&gt;
		&lt;!-- Este archivo es para darle un estilo (Este archivo es Opcional) --&gt;
	    &lt;script type="text/javascript" src="js/themes/grid.js"&gt;&lt;/script&gt;
		&lt;!-- Este archivo es para poder exportar los datos que obtengamos --&gt;
		&lt;script type="text/javascript" src="js/modules/exporting.js"&gt;&lt;/script&gt;
	
		&lt;script type="text/javascript"&gt;
		
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Ranking de Navegadores, 2011'
					},
					tooltip: {
						formatter: function() {
							return '&lt;b&gt;'+ this.point.name +'&lt;/b&gt;: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: false
							},
							showInLegend: true
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',
						data: [
							['Firefox',   45.0],
							['IE',       26.8],
							{
								name: 'Chrome',    
								y: 12.8,
								sliced: true,
								selected: true
							},
							['Safari',    8.5],
							['Opera',     6.2],
							['Others',   0.7]
						]
					}]
				});
			});
				
		&lt;/script&gt;
		
	&lt;/head&gt;
	&lt;body&gt;
		
	
		&lt;div id="container" style="width: 800px; height: 400px; margin: 0 auto"&gt;&lt;/div&gt;
		
				
	&lt;/body&gt;
&lt;/html&gt;
&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"&gt;
&lt;html&gt;
	&lt;head&gt;
	 &lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt;
	   &lt;title&gt;Estadisticas con Jquery | Jquery Easy&lt;/title&gt;
		&lt;script type="text/javascript" src="js/jquery-1.4.2.min.js"&gt;&lt;/script&gt;
		&lt;script type="text/javascript" src="js/highcharts.js"&gt;&lt;/script&gt;
		&lt;!-- Este archivo es para darle un estilo (Este archivo es Opcional) --&gt;
	    &lt;script type="text/javascript" src="js/themes/grid.js"&gt;&lt;/script&gt;
		&lt;!-- Este archivo es para poder exportar los datos que obtengamos --&gt;
		&lt;script type="text/javascript" src="js/modules/exporting.js"&gt;&lt;/script&gt;
	
		&lt;script type="text/javascript"&gt;
		
			var chart;
			$(document).ready(function() {
				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Ranking de Navegadores, 2011'
					},
					tooltip: {
						formatter: function() {
							return '&lt;b&gt;'+ this.point.name +'&lt;/b&gt;: '+ this.y +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: false
							},
							showInLegend: true
						}
					},
				    series: [{
						type: 'pie',
						name: 'Browser share',
						data: [
							['Firefox',   45.0],
							['IE',       26.8],
							{
								name: 'Chrome',    
								y: 12.8,
								sliced: true,
								selected: true
							},
							['Safari',    8.5],
							['Opera',     6.2],
							['Others',   0.7]
						]
					}]
				});
			});
				
		&lt;/script&gt;
		
	&lt;/head&gt;
	&lt;body&gt;
		
	
		&lt;div id="container" style="width: 800px; height: 400px; margin: 0 auto"&gt;&lt;/div&gt;
		
				
	&lt;/body&gt;
&lt;/html&gt;