<?php 
include_once('../../modelo/clsComboBox.php');

extract($_REQUEST);
if (isset($accion)) {
	$accion = $_GET['accion'];
}
//carga de profesión u Oficio
if ($accion == 1) {
	$profesion = new ComboBox();
	$profesiones = $profesion->cargarProfesion();
	$row =  array();
	while($arrelo =  pg_fetch_assoc($profesiones)){
		$id = $arrelo ['idprofecionoficio'];
		$valor = $arrelo ['profecionoficio'];
		$row[$id] = $valor;
		};
	foreach($row as $key => $valor){
	echo "<option value='$key'>$valor</option>";
	}
}
//carga de Grado
if ($accion == 2) {
	$grado = new ComboBox();
	$grados = $grado->ComboBoxGrado();
	$row = array();
	while($arrelo = pg_fetch_assoc($grados)){
		$id = $arrelo ['idgrado'];
		$valor = $arrelo ['grado'];
		$row[$id] = $valor;
		};
	foreach($row as $key => $valor){
	echo "<option value='$key'>$valor</option>";
	}
}
//carga de Seccion
if ($accion == 3) {
	$seccion = new ComboBox();
	$secciones = $seccion->ComboBoxSeccion();
	$row = array();
	while($arrelo = pg_fetch_assoc($secciones)){
		$id = $arrelo ['idseccion'];
		$valor = $arrelo ['seccion'];
		$row[$id] = $valor;
		};
	foreach($row as $key => $valor){
	echo "<option value='$key'>$valor</option>";
	}
}
//carga de Turno
if ($accion == 4) {
	$turno = new ComboBox();
	$turnos = $turno->ComboBoxTurno();
	$row = array();
	while($arrelo = pg_fetch_assoc($turnos)){
		$id = $arrelo ['idturno'];
		$valor = $arrelo ['turno'];
		$row[$id] = $valor;
		};
	foreach($row as $key => $valor){
	echo "<option value='$key'>$valor</option>";
	}
}
?>